Changes in lyskomd 2.1.2 (Release date 2003-08-30)
--------------------------------------------------

* The server crashed if a person has read more than 65535 texts
  after the first unread text.

* The internal data structure for handling read texts could be
  damaged, resulting in overlapping ranges in the read-ranges part of
  a Membership.  This version fixes the bug, and automatically fixes
  the damage in the database.  You don't need to run dbck.

* The mx-refuse-import aux-item has a new supported value: "html".

* A few machine-readable versions of the protocol specification has
  been extracted.  See Protocol-A.texi for details.

* The garb process is now documented in Protocol-A.texi.

* The garb was running too slowly in version 2.1.0 and 2.1.1.


Changes in lyskomd 2.1.1 (Release date 2003-08-26)
--------------------------------------------------

* The server could crash when a client disconnects.  The problem was
  introduced in version 2.1.0.  All 2.1.0 installations should upgrade
  to 2.1.1 as soon as possible.

* A few auxiliary files was missing from the distribution, most
  notably src/libraries/adns/changelog.

* The file "aux.h" has been renamed to "aux-no.h".  Note: this does
  not mean that Cygwin is a recommended environment.

* The exportdb directory, used by splitkomdb, is now created during
  "make install".

* The installation instructions forgot a step, so when upgrading the
  server would never start if the instructions were followed
  literally.


Changes in lyskomd 2.1.0 (Release date 2003-08-24)
--------------------------------------------------

* Protocol changes: (changes that could affect clients)

** add-comment and add-footnote can both return the error codes
   already-comment and already-footnote.  This ensures that a text
   cannot be both footnote and comment to the same text at the same
   time.

** The text garb has been modified, so that a fresh comment or
   footnote to a text prevents the text from being removed.  The
   amount of extra time the text lives depends on the keep-commented
   field if the conference status, and defaults to 77 days.  The
   set-keep-commented request that was previously labeled
   "Experimental" is now labeled "Recommended", as it actually does
   something useful now.

** You can tell the server that you don't want to receive group
   messages for a group, via the new passive-message-invert bit of the
   Membership-Type.

** The following requests can no longer be used until you have logged
   in:

	58=get-last-text
	60=find-next-text-no
	61=find-previous-text-no

** You can now modify the type of a recipient (with the add-recipient
   request) if you are the supervisor of either the author, recipient
   or sender.  The check used to be more restrictive.

** The following asynchronous messages are now sent to recipients of
   texts linked to the relevant text: async-deleted-text,
   async-new-text, async-new-text-old, async-add-recipient and
   async-sub-recipient.

** New aux-item: 35=mx-refuse-import.

** The supplied aux-items.conf file now validates many aux-items more
   strictly.

** If the client sends a number apart from 0 or 1 where a BOOL
   argument is expected, the server now responds with the new error
   code bad-bool.

* Protocol extensions: (should not affect old clients)

** The information about which texts you have read is now transmitted
   in a much more efficient form: as a list of ranges.  Previously, it
   was transmitted as a number that you have ready everything before,
   and a list of additional text that you have read.  It is now
   possible to mark a text as not read, and to efficiently tell the
   server exactly which texts should be marked as read.  Affected
   requests:

        107=query-read-texts
        108=get-membership
	109=mark-as-unread
        110=set-read-ranges

   The old requests 98=query-read-texts-10 and 99=get-membership-10
   will continue to work, but clients are encouraged to switch to the
   new requests instead.

** The server now keeps real-time statistics about a lot of things,
   such as the number of pending DNS requests, the number of connected
   clients, the number of processed requests, the size of various
   buffers, et c.  You can also get the ascent and descent rates for
   all measured variables.

        111=get-stats-description
        112=get-stats

** The server can tell when it last started, how many texts existed
   then, and some other information that was current when it started.

        113=get-boottime-info

** Three new requests makes it possible to loop through all existing
   conferences without having to guess what the largest conference
   number is, and without having to try each number in turn.

        114=first-unused-conf-no
        116=find-next-conf-no
        117=find-previous-conf-no

** The highest text number can now be retrieved.

        115=first-unused-text-no

** The times can now be expressed in UTC instead of in the local time
   zone of the server.  This makes it possible to write a client that
   displays time in the local time zone of the client, and makes it
   easier to correctly format dates as "today" or "yesterday" even
   when the client and server are in different time zones.

        120=set-connection-time-format

** In case you want to traverse texts in a conference (or written by
   an author) in the direction  from newer to older texts, you can now
   do so efficiently with these new requests:

        121=local-to-global-reverse
        122=map-created-texts-reverse

   (The field later-texts-exists in Text-Mapping has been renamed to
   more-texts-exists, so that it makes sens when these new requests
   are used.)

** The server is now much more fair in giving each client
   approximately the same amount of resources while under heavy load.
   Clients can use these new requests to tell the server that they
   want more or less than their fair share, by adjusting their weight.
   (Compare with the "nice" command of Unix.)

        118=get-scheduling
        119=set-scheduling

   These commands can also set the priority.  As long as a client of
   priority N has a request for the server, clients with a priority
   larger than N will be blocked.  However, the current implementation
   only supports a single priority.

   The scheduling requests are considered experimental.

** A few new asynchronous messages have been added:

        19=async-new-user-area
        20=async-new-presentation
        21=async-new-motd
        22=async-text-aux-changed

* Client-visible bugs fixed:

** The session_start field of a Connection was used both to record the
   connect time and the login/logout time.  As a result, the
   connection-time of a Static-Session-Info was not actually static.
   Fixed by keeping track of the connect time and the login/logout
   time separately.

** It is now impossible to add the same text as FAQ for the same
   conference more than once.

** The code that determined who is allowed to remove or add an
   aux-item from a conference or person was wrong.

** Idle clients are now disconnected after a (long) timeout.  The
   timeout is different during the differen phases of a connection.
   The following parameters (with the default value) affects this:

	Parameter	 Default	Meaning
	=========	 =======	=======

	Stale timeout	 60 minutes	Output buffer full and client
					not reading

	Connect timeout  30 seconds	Idle timeout before sending
					initial handshake.

	Login timeout	 30 minutes	Idle timeout while not logged
					in.

	Active timeout	 11.5 days	Idle timeout while logged in.

   The idle timer is reset whenever anything is written to the
   client.

** If a client sends a 27=mark-as-read request with a too long array,
   it will now receive the error code long-array instead of a
   notification of a protocol error.

** If the output buffer to a client becomes too large the server will
   stop reading from that client until some of the output is drained.
   The following new parameters affects when the output buffer is
   considered to be too large:

	Max client transmit queue messages
	Max client transmit queue bytes

** In some cases, memcpy() was used to move memory within a buffer,
   from a higher to a lower address.  The source and destination could
   overlap.  This might have caused the server to send garbage to
   clients in rare occasions, since it is implementation-defined what
   happens if memcpy() is used with overlapping source and
   destination.

* Other client-visible changes:

** The server is now more careful never to reuse text, person and
   conference numbers, even after a crash.

** 27=mark-as-read no longer leaks information about secret
   conferences.

** IPv6 support has been contributed by Thorild Sel�n.  (Changes made
   in the last few months may however have broken the code, so it
   should be considered experimental.)

* Protocol specification bugs fixed:

** The descripton of the error code index-out-of-range was wrong for
   some requests.  See the "Document Edition History" for details.

** async-leave-conf is not sent when a person is deleted.

* Installation changes:

** A serious bug in the SIGWINCH handling has been found but not
   fixed.  Please see lyskomd.texi before you send a SIGWINCH signal
   to the server!

** The file system layout has changed.  The following files and
   directories have been moved.  As a result, it should now be safe to
   install lyskomd with prefix set to /usr/local or even /usr.  The
   default prefix is still /usr/lyskom, however.  See README for
   upgrade instructions.  You should be able to change the file names
   via standard "./configure" arguments, such as --localstatedir.  See
   INSTALL.

Parameter	    Old default value       New default value
=========           =================       =================

lyskomd path:       bin/lyskomd             sbin/lyskomd
savecore path:      bin/savecore            sbin/savecore-lyskom
Data file:          db/lyskomd-data         var/lyskomd/db/lyskomd-data
Backup file:        db/lyskomd-backup       var/lyskomd/db/lyskomd-backup
Backup file 2:      db/lyskomd-backup-prev  var/lyskomd/db/lyskomd-backup-prev
Lock file:          db/lyskomd-lock         var/lyskomd/db/lyskomd-lock
Text file:          db/lyskomd-texts        var/lyskomd/db/lyskomd-texts
Number file:        db/number.txt           var/lyskomd/db/number.txt
Number temp file:   db/number.tmp           var/lyskomd/db/number.tmp
Text backup file:   db/lyskomd-texts-backup var/lyskomd/db/lyskomd-texts-backup
Status file:        etc/status              var/lyskomd/db/status
Log file:           etc/server-log          var/lyskomd.log
Log statistics:     etc/lyskomd-log         var/lyskomd.stats
Memory usage file:  etc/memory-usage        var/lyskomd.memory
Core directory:     cores                   var/lyskomd.cores
Pid file:           etc/pid                 var/run/lyskomd.pid
Connection status file: 
		    etc/connections.txt     var/lyskomd.clients
Connection status temp file: 
		    etc/connections.tmp     var/lyskomd.clnt.tmp
Backup export directory: 
                    exportdb                var/lyskomd/exportdb

    All other binaries have also been moved from "bin" to "sbin".

** lyskomd is now officially shut down via SIGTERM.  (SIGHUP still
   works, and SIGINT now also shuts it down cleanly).  In a future
   release, SIGHUP will probably change meaning to "re-read the
   configuration files".

** This release was made using autoconf-2.57 and automake-1.7.6 (with
   a small patch).

** The following new parameters affect how client scheduling works:

	Penalty per call
	Penalty per read
	Max penalty
	Low penalty
	Default priority
	Max priority
	Default weight
	Max weight

   See lyskomd.texi for documentation on the parameters.

** The following parameters have been removed:

	Idle timeout
	Max client transmit queue

** Most parameters that specify a time period can now accept a suffix
   that specifies the unit.  If the unit is not given, it defaults to
   the old unit, which are different for different parameters.
   Affected parameters:

	Garb busy postponement
	Garb timeout
	Sync timeout
	Garb interval
	Sync interval
	Sync retry interval
	Stale timeout
	Connect timeout
	Login timeout
	Active timeout

** When updateLysKOM sends an email, it now uses the "sendmail"
   program.  The path is found at configure time, and can be
   overridden with the parameter "sendmail path".  The special value
   ":" turns off mail delivery.

** The following parameters not mentioned elsewhere have been added:

	Garb busy postponement
	Max read_ranges per call
	Number file
	Number temp file
	Statistic name length
	Max client message size

* Bug fixes

** The "Max texts" and "Max conferences" parameters were off by one.

** The server now works on IRIX64 and FreeBSD 4.7.

** The server now stores almost all times and time spans as "struct
   timeval" or "time_t".  This eliminates a number of potential
   overflows.

* Other noteworthy improvements:

** The format of the var/lyskomd.clients file is now documented.

** The server now uses liboop for the event loop.  As a result, libisc
   has been heavily mutilated.  Its main function is now to provide
   output buffering and to handle the details of TCP connection
   establishment (over both IPv4 and IPv6).

   The server comes with its own version of liboop.  Some
   modifications had to be made.  (They have been sent to the
   maintainer.)

** The server now uses adns to resolve IP addresses in an asynchronous
   way.  (Since adns doesn't support IPv6 addresses, this only works
   for IPv4 addresses.  The old blocking method is used for IPv6
   addresses.)  A client that connects can do the initial handshake,
   up to the point of receiving the string "LysKOM", but after that
   point the client will be blocked until the DNS resoultion
   completes.  The client will be totally invisible to other clients
   until the DNS resolution completes (so that the result of
   get-static-session-info really is static).

** Some general code cleanup and a few small bug fixes has been made.


Changes in lyskomd 2.0.7 (Release date 2002-11-03)
--------------------------------------------------

* Protocol changes: (changes that affect clients)

** The aux-item send-comments-to has been extended.  The value may now
   contain an optional recipient type.

** The rules for when a membership is visible is now documented in the
   protocol specification.  Previously, different requests used
   different rules.  They are now fixed:

*** query-read-texts, query-read-texts-old and sub-member could return
    the wrong error code in several instances.

*** get-membership and get-membership-old didn't always honor the
    unread-is-secret flag of a person.

*** get-members and get-members-old could leak information about
    secret persons.

*** get-unread-confs was very confused about what memberships it
    returned.

* Other client-visible changes:

** If the server fails to save the database, a broadcast message is
   sent to everyone that is logged in.  (As always, an entry is also
   made in the log file.)

* Protocol specification bugs fixed:

** The documentation for the common block of the user area was just
   plain wrong.

** The documentation for no-of-created-texts in the Person structure
   was wrong.

** The documentation for the privilege bits create-conf and create-pers
   was wrong.

** The Protocol A examples for re-z-lookup and lookup-z-name were
   wrong.

* Bug fixes that may affect clients:

** According to the documentation, get-membership and
   get-membership-old should always have returned the array size even
   when the want-read argument is false.  However, they always
   reported the array size as 0 in that case.

* Protocol specification improvements:

** The documentation for get-person-stat-old was improved.

** Documented the unread-is-secret flag of a person.

** The fact that the last line in a text should not be terminated by a
   linefeed character is now documented.

** A recommendation on the ordering of misc-info groups has been
   added.

** A new informative appendix that documents some client-specific
   aux-items has been added.  elisp-client-read-faq and
   elisp-client-rejected-recommendation are now documented in that
   appendix.

** Added a recommendation that clients should offer to add the author
   as a recipient, when the author isn't a member of any of the
   recipients.

** A few typos were fixed.

* Protocol extensions:

** Added a new "language" value to the common block.

** The Message-ID of exported texts is now documented.

** New aux-item: world-readable.

** New client-specific aux-items: elisp-client-read-faq and
   elisp-client-rejected-recommendation.

* Bug fixes: (not visible by clients)

** If a client disconnects after sending the length of an ARRAY
   Aux-Item-Input, but before sending the "{", the server would
   crash.

** We now use Xenofarm to compile and test lyskomd automatically on a
   compile farm of several different machines.  Several problems have
   been fixed, so this release should be more portable than previous
   versions.  Read more about Xenofarm at 

      http://www.lysator.liu.se/lyskom/lyskom-server/xenofarm.html

** The code has been cleaned up so that many compiler warnings are
   gone.  (The regex.c file still generates a lot of warnings.)

** Several minor bugs were fixed, and the code was cleaned up in
   several ways.  See the ChangeLog for all the details.

** The test suite has been extended, primarily with tests for
   membership visibility.

** The test suite should now work on more hosts.  It makes a few less
   assumptions.  It is still not as portable as the rest of the
   distribution, however.

** Several race conditions in the test suite have been eliminated.

** valgrind has been used to ensure that there are no memory leaks or
   memory access errors.  A few minor problems were found and fixed.
   (A couple of minor leaks still remains.)  The configure options
   --with-valgrind and --disable-malloc-guards are useful when you want
   to run the test suite using valgrind.  (Don't use
   --disable-malloc-guards on a production use server.  Due to bug 691
   it might cause crashes.)

** The "Open files:" setting now works even on hosts that don't have
   setrlimit, or where setrlimit cannot reduce the number of open
   files.

** lyskomd now refuses to use more than FD_SETSIZE file descriptors.

* Improvements: (not visible by clients)

** The new file etc/connections.txt contains a listing of all
   sessions, what file descriptor they use, and from where they are
   connected.  If a runaway client hogs the server, the administrator
   can use tools such as strace or truss to find the file descriptor,
   and the file descriptor can then be looked up in this file.  The
   name of the file can be changed via the "Connection status file:"
   configuration parameter.

** DNS lookups blocks the server.  If a lookup takes more than 1.5
   seconds (configurable via "DNS log threshold:") an entry is made in
   the log.  DNS lookups can also be disabled via "Use DNS:".

** The "-d" option no longer causes lyskomd to run in the foreground;
   use the new "-f" option for that purpose.  By specifying only "-f"
   and not "-d", it is now possible to run lyskomd in the foreground
   without increasing the log level.  The test suite now uses this to
   check what lyskomd writes to the log file.

** NFS-mounted databases might fail under some circumstances.  A
   warning about that fact has been added to the README file.  This
   might be fixed in a future release.


Changes in lyskomd 2.0.6 (Release date 2002-03-29)
--------------------------------------------------

* New aux-item: send-comments-to.

* Modified aux-item: faq-text may now be set on a letterbox.

* The Protocol A documentation has been enhanced.  See the document
  revision in doc/Protocol-A.texi.  It can now be formatted as PDF and
  DVI.  Info is still the best-tested output format.

* The splitkomdb utility is now documented.  It is useful for taking
  live backups of the lyskomd data base.

* We now use http://bugzilla.lysator.liu.se/ to track bugs and feature
  requests in lyskomd.  All old FIXME comments, TODO files, et c have
  been entered into that system.  A few test cases are expected to
  fail; they now refer to the corresponding entry in the Bugzilla
  system.

* Many minor bugs and potential portability problem have been fixed,
  and the code has been cleaned up in several places.  See the
  ChangeLog for details.  lyskomd should now compile under Cygwin.

Changes in lyskomd 2.0.5 (Release date 2001-09-30)
--------------------------------------------------

* New aux-items: mx-mime-belongs-to, mx-mime-part-in, mx-mime-misc,
  mx-envelope-sender, mx-mime-file-name, canonical-name,  mx-list-name.

* The supervisor of the object that certain aux-items are attached to
  can now remove the aux-item.  Affected aux-items are redirect,
  x-face, pgp-public-key, e-mail-address, faq-text and
  allowed-content-type.  This is configured via the new owner-delete
  attribute of aux-items.conf.

* A bug that caused async-new-recipient to not be sent in some
  cases when passive memberships were involved has been fixed.

* The test suite has been updated to use DejaGnu 1.4.2.  It has also
  been slightly expanded.

Changes in lyskomd 2.0.4 (Release date 2000-09-09)
--------------------------------------------------

* New aux-items: allowed-content-type and recommended-conf.

* The distributed aux-items.conf will silently overwrite any
  previously installed aux-items.conf file when "make install" is run.
  Take care if you have edited your local copy.

* Information about bcc-recpts was sometimes sent when too often, and
  sometimes too seldom.  This has been fixed.

* The conference number of secret conferences could leak when
  information about the current working conference of a user was sent
  to another user.  This has been fixed.

* The garb-nice value is internally converted from days to seconds.
  That conversion previously used integer arithmetic, so it could
  overflow.  This has been fixed.

* If the config file contained "Never save: true" the data base was
  nevertheless saved if lyskomd terminated normally.

* A bug that could cause a crash when a client sent over-long aux-item
  lists to the server has been fixed.

* Sending a SIGWINCH signal to the server will cause it to re-read the 
  aux-item definition file.

Changes in lyskomd 2.0.3 (Release date 1999-07-25)
--------------------------------------------------

* The 2.0.2 release didn't really fix the bug.  This release includes
  a test case that was triggered by the bug, and at least now that
  test case succeeds.  As usual, there are no guarantees, but the bug
  should be fixed now.

Changes in lyskomd 2.0.2 (Release date 1999-07-23)
--------------------------------------------------

* Fixed a bug that caused lyskomd to follow a NULL pointer when
  an empty name was looked up.

Changes in lyskomd 2.0.1 (Release date 1999-07-12)
--------------------------------------------------

* Protocol changes (changes that affect clients)

** Regexp matching is never ever case insensitive.  The behavior of
   regexp matching in release 2.0.0 was severely broken.  We will not
   change this again.

** The author of a text is allowed to change the recipient type (to
   recpt, cc-recpt or bcc-recpt) with the add-recipient call.
   Previously, only the supervisor of the recipient could do this.

* Administration and installation changes

** No asynchronous messages are sent to the clients during shutdown of
   the server.  This speeds up the shutdown slightly.

* Bug fixes

** A buffer overrun and several minor bugs were fixed.

** The created_texts field of persons were not always properly updated
   when texts were deleted.  You may have to run dbck to fix your
   database.

** Changes made to the Conference were sometimes forgotten and not
   saved to disk.  dbck can not yet fix all the errors that may have
   been introduced.

* Performance fixes

** The calls that used regexps were very slow.  The cause was not the
   actual regular expression matching, but that the permission
   checking was done in a broken way.  This has been fixed, and should
   improve performance on many of the calls, but especially for the
   regexp lookup functions.

** The calls that create, change or delete names of persons and
   conferences are now much faster.

** Copying data from lyskomd-backup to lyskomd-data no longer uses
   dynamically allocated memory.

Changes in lyskomd 2.0.0 (Release date 1999-06-27)
--------------------------------------------------

* Protocol changes (changes that affect clients)

** It is now possible to see when a membership was added and by whom.
   Memberships come in several different flavors through the addition
   of a membership type. Currently defined types are secret, passive
   and invitation.

   Since protocol version 9 setting a priority of zero in a conference
   was supposed to indicate passive membership in a conference.  It
   was largely up to the client to implement this. True passive
   memberships have been introduced in this protocol version through
   the Membership-type extension to the Membership type. In order to
   maintain compatibility with clients that interpret priority 0 as
   passive membership, the old calls perform magic, translating
   between priorities and membership types.  Clients that use the new
   calls can now once again use priority 0.

   These new calls include the Membership-type:

        98=query-read-texts
        99=get-membership
        100=add-member
        101=get-members
        102=set-membership-type

** Error reporting from the server has been improved. The possible
   error codes from each call have been checked and documented. In the
   process some calls now return errors they previously did not. Since
   the error returns were never defined, this should not present a
   problem for existing clients. The error-status field is set to
   something semi-useful in all cases.

** Several situations where the server leaked secret information have
   been fixed.

** The conference type has been extended so that it is possible to
   forbid secret members in the conference.  All old calls that wanted
   a conference type now accepts both the old-style type and the
   new-style.  This call returns the new conference type:

        91=get-conf-stat

** Regexp matching is now case insensitive by default. Case sensitivity
   is configurable via a config file for the server.  This is an
   incompatible change.

** It is now possible to attach arbitrary data to conferences and
   texts. This allows simple extensions to server data structures
   without requiring protocol changes. These data object are called
   aux-items, and are configured with the aux-items.conf file. There
   are several predefined aux-items with special meanings; see
   doc/Protocol-A.texi.  These new calls handle aux-items:

        86=create-text
        87=create-anonymous-text
        88=create-conf
        89=create-person
        90=get-text-stat
        92=modify-text-info
        93=modify-conf-info
        94=get-info
        95=modify-system-info
        96=query-predefined-aux-items


** A new recipient type, BCC recipient, has been added. BCC recipients
   are only shown to potential members of the recipient and to the
   sender.  The server accept bcc-recpt on input everywhere where it
   previously accepted recpt or cc-recpt.  This call can return
   bcc-recpt:

        90=get-text-stat

** The server can send asynchronous messages about texts being deleted;
   recipients being added and removed; and memberships being added.
   This should help clients with caches to keep them updated.

** There is a call to get the collate table being used by the server.

        85=get-collate-table

** A call to set the flags field in a person structure has been added.
   The new version of create-person also takes flags as a parameter.

        89=create-person
        106=set-pers-flags

** There is a new parameter (keep_commented) on conferences for tuning
   the garbage collection process. This tuning is not implemented in
   the server, but users and clients are encouraged to start using the
   tuning parameter anyway since the tuning will be implemented in a
   future release.

        91=get-conf-stat
        105=set-keep-commented

** There is a parameter (expire) for configuring automatic removal of
   inactive conferences. This removal is currently not implemented, but
   users and clients are encouraged to start using it anyway and the
   automatic conference deletion procedure will be implemented in the
   near future.

        91=get-conf-stat
        97=set-expire

** Mapping of local text numbers to global text numbers is now much
   more efficient, which should lead to significant performance
   improvements if clients make use of these facilities.

        103=local-to-global
        104=map-created-texts

* Administration and installation changes

** The database format has changed again. Old installations must
   convert their old database--see README for instructions.

** The komrunning and updateLysKOM utilities are now a C programs that
   read the configuration file, instead of shell scripts.  komrunning
   now takes "start" or "stop" as argument instead of "up" and "down".

** The undocumented format of the etc/lyskomd.log file, which stores
   usage statistics, has been changed.

** The server can now reliably handle a lot more than 250 simultaneous
   clients even platforms where a FILE* only can refer to file
   descriptor 0-255.

** You can specify which IP number the server should listen to.  The
   default is still to bind all IP numbers of the host that runs
   lyskomd.

** Support for the MUX protocol has been removed.

** The database is now locked with a lock symlink, so there is less
   risk of accidentally running a lyskomd and dbck or two lyskomd at
   the same time.

* Bug fixes

** Sanity checks in database and protocol code increase stability and
   resilience to client errors and sabotage.

** Portability fixes. This release compiles cleanly on systems such as
   modern Linux systems that define errno as a macro.

* Misc

** Documentation has been moved to two Texinfo files, one that
   specifies LysKOM Protocol A and one that documents lyskomd. The man
   pages are no longer maintained.

** The protocol documentation is more detailed, converted to Texinfo and
   written in English.

** The distribution contains a test suite that is used for regression
   testing the server in development. The test suite should run with no
   unexpected failures.

** We now use GNU automake.

** GNU malloc is no longer included in the distribution.

** Updated to use libisc 1.00 (with a few minor modifications; see
   src/libraries/libisc-new/ChangeLog).


Changes in lyskomd 1.9.0 (Release date 1996-08-04)
--------------------------------------------------

* The database format has changed.  Old installations must convert
  their old database -- see INSTALL for instructions.  The database
  now stores information about which text that is the
  message-of-the-day, so that information no longer has to be set in
  the config file.

* dbck now uses GNU getopt, and can take long options.  See the man page
  or "dbck --help".

* dbck can now be used to reset the password of a single user to the
  empty string (using the new --clear-password option) or give a user
  all available privileges.  This is handy if you forget the password
  of the LysKOM user with all privileges, or if you happen to delete
  that person by mistake.

* Portability fixes.  This release of lyskomd has been compiled with 4
  different compilers on 8 different CPU:s using 9 different OS:s.  3
  different make programs have been used.

* The server supports idle-time handling using 3 new calls.  Clients
  are expected to use this new functionality shortly.

* Anonymous texts are still allowed per default, but it is now
  possible to set a bit in the conference type of a conference so that
  anonymous texts are rejected.

* New simplified rules for when adding and subtracting comment and
  footnote links is allowed.

* Clients can now tell the server which asynchronous calls they are
  interrested in receiving.  This change can potentially decrease the
  network bandwidth used by lyskomd in the future.

* Forward compatibility: the server now returns a normal error code
  when the client attempts to use an unsupported call.  The client can
  now reliably try new calls and see if the server supports them.

* Several bug fixes.  The most important is probably that lyskomd
  should now stop cleanly when a HUP is received, even if a second HUP
  signal is received while it is going down.  This didn't work on all
  platforms.


Changes in lyskomd 1.8.0 (Release date 1995-11-08)
--------------------------------------------------

* If configured with --with-gnu-malloc lyskomd will use the GNU malloc
  package instead of the system-supplied malloc.  GNU malloc is
  included in the distribution.

* The asynchronous message async_rejected_connection is now sent at
  most once per minute.

* Lyskomd 1.8.0 implements protocol A level 8.  See doc/prot-A.txt for
  information about the new features it contains.  The database
  currently still only stores four bits of the conf_type, however, so
  it is useless to use the new anarchy bit until the next release.

* You can now specify where lyskomd should dump core (if that should
  ever happen) using the "Core directory" parameter.

* Can now handle 1500000 texts and 4765 persons/conferences.

* Text garbing can now be turned off completely from the config file.

* dbck -t now dumps a list of all existing text numbers to stdout.

* Some minor portability fixes.  Some bug fixes.  See the various
  ChangeLogs for details.

Changes in lyskomd 1.7.1 (Release date 1995-01-08)
--------------------------------------------------

* doc/prot-A.txt is improved.

* Minor portability changes to make lyskomd compile under Solaris 2.4,
  Ultrix 4.4 and Dynix 3.0.14.

* Fixed bugs that caused "./configure;make;make distclean" to fail to
  remove all created files.

* The default installation prefix is now, once again, /usr/lyskom.
  By accident it was changed to /usr/local in 1.7.0.

Changes in lyskomd 1.7.0 (Release date 1994-12-31)
--------------------------------------------------

* New calls "re_z_lookup" and "lookup_z_name" allows client to list
  conference names *fast*.

* New call "get_version_info" returns information about the protocol
  level implemented by the server, the server name, and the server
  version number.

* The call "send_message" now accepts a conference as recipient, and
  will direct the message to all members of that conference that are
  currently logged on.

* The file doc/prot-A.txt is updated.  The current protocol version is
  (somewhat arbitrarily) defined as 7.

* If the file system holding the LysKOM database was full while
  somebody tried to create a text strange things could happen.  Error
  checking has been added, and the client will now receive a
  KOM_TEMPFAIL if there is not enough space left on the device.

* The 1.6.6 distribution contained some junk files, and others (such
  as the global ChangeLog) were missing.  This has now been fixed.

* Autoconf 2.1 has generated the current configure script.

* The obsolete asynchronous message i_am_off is no longer sent.

Changes in lyskomd 1.6.6 (Release date 1994-11-15)
--------------------------------------------------

* Can now handle 1000000 texts and 4765 persons/conferences.

* mark_as_read can (hopefully) no longer cause the server to crash.

Changes in lyskomd 1.6.5 (Never released, used since 1994-10-24)
-----------------------------------------

* New configuration parameter "Open files" can be used on hosts that
  support setrlimit to increase the limit on simultaneous connections.

* New configuration parameter "Force ISO 8859-1" assists in
  environments where setlocale() is broken or lacks support for
  ISO 8859-1.

* Slightly improved configure.in script.

* Many harmless warnings and some errors from apcc removed.

* NEWS entry for 1.6.4 was added.

Changes in lyskomd 1.6.4 (Release date 1994-06-30)
--------------------------------------------------

The call get-last-text now works.  It has been broken since 12 oct
1993.

It is now impossible to add a text to a conference unless the sender
is able to enter texts into it.

Changes in lyskomd 1.6.3 (Release date 1994-06-20)
--------------------------------------------------

One bug was fixed: it is now possible to compile src/server/simple-cache.c.

Changes in lyskomd 1.6.2 (Release date 1994-06-19)
--------------------------------------------------

This is a bug-fix release.

* The parameter ``Default change name capability'' was ignored, so any
  person created with release 1.6.1 of the server cannot change his
  name.  Use "dbck -r -c" to set the bit for everyone.

* The -c option to dbck is new.

* The parameter ``Ident-authentication'' now works, so it is now
  possible to turn off ident authentication.

* find_previous_text_no is now much more efficient if the client tries
  to search for a very large number.  Version L0.12 of the tty-client
  will probably use find_previous_text_no(MAX_TEXT_NO), so this fix is
  important.

For older news, see the various ChangeLog files.
