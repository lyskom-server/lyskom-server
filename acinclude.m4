dnl $Id: acinclude.m4,v 1.11 2003/08/23 16:38:23 ceder Exp $
dnl Copyright (C) 1994-1995, 1998-1999, 2001-2003  Lysator Academic Computer Association.
dnl
dnl This file is part of the LysKOM server.
dnl 
dnl LysKOM is free software; you can redistribute it and/or modify it
dnl under the terms of the GNU General Public License as published by 
dnl the Free Software Foundation; either version 1, or (at your option) 
dnl any later version.
dnl 
dnl LysKOM is distributed in the hope that it will be useful, but WITHOUT
dnl ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
dnl FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
dnl for more details.
dnl 
dnl You should have received a copy of the GNU General Public License
dnl along with LysKOM; see the file COPYING.  If not, write to
dnl Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
dnl or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
dnl MA 02139, USA.
dnl
dnl Please report bugs at http://bugzilla.lysator.liu.se/. 
dnl

dnl
dnl Check if an option is acceptable to the C compiler in use.
dnl (This is taken verbatim from cmod 1.2.  Please don't make even a
dnl tiny change to it unless you change the name of all variables!
dnl See the cmod source code for more information.)
dnl
AC_DEFUN([CMOD_CHECK_CC_OPT],
[AC_MSG_CHECKING([whether ${CC} accepts $1])
AC_CACHE_VAL([cmod_cv_compiler_]$2,
[[cmod_oldflags=$CFLAGS
CFLAGS="$CFLAGS $1"]
AC_TRY_LINK(,,
	[cmod_cv_compiler_]$2[=yes],
	[cmod_cv_compiler_]$2[=no])
[CFLAGS=$cmod_oldflags]])dnl
AC_MSG_RESULT([$cmod_cv_compiler_]$2)
if test [$cmod_cv_compiler_]$2 = yes; then
  CFLAGS="$CFLAGS $1"
fi])dnl
dnl
dnl Another frozen defun.
dnl
AC_DEFUN([CMOD_C_WORKING_ATTRIBUTE_UNUSED],
[AC_CACHE_CHECK([[whether $CC understands __attribute__((unused))]],
    [[cmod_cv_c_working_attribute_unused]],
    [dnl gcc 2.6.3 understands the __attribute__((unused)) syntax
    dnl enough that it prints a warning and ignores it when the
    dnl variable "i" is declared inside the function body, but it
    dnl barfs on the construct when it is used in a
    dnl parameter-declaration.  That is why we have a function
    dnl definition in the prologue of AC_LANG_PROGRAM part.
    AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM([[int cmod_x(int y __attribute__((unused)))
			   { return 7; }]],
	    [[int i __attribute__((unused));]])],
	[cmod_cv_c_working_attribute_unused=yes],
	[cmod_cv_c_working_attribute_unused=no])])
[if test $cmod_cv_c_working_attribute_unused = yes ; then]
    AC_DEFINE([HAVE_ATTRIBUTE_UNUSED], [1], 
	      [Define if your compiler supports __attribute__ ((unused)).])
[fi]])dnl
