# Bind a TCP port.
# Copyright (C) 1998-1999, 2001-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
#
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 1, or (at your option)
# any later version.
#
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge,
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/.

import socket
import sys

s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
port=int(sys.argv[1])
try:
   s.bind(('', port))
except socket.error as x:
   print("tcpbind: bind failed:", x[1])
   sys.exit(1)
s.listen(1)
print("socket bound")
sys.stdout.flush()
sys.stdin.readline()
print("bye")
