# Test suite for lyskomd.
# Copyright (C) 1999, 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 


# Additional test cases for session.c
 
source "$srcdir/config/prot-a.exp"
read_versions

proc startup {} {
    lyskomd_start

    client_start 0
    talk_to client 0
    kom_connect "DejaGnu test suite"
    kom_accept_async "0 { }";

    kom_login 5 "gazonk" 0

    kom_create_person "QERSON 6" "" "00000000" "0 { }"
    kom_create_person "qerson 7" "" "00000000" "0 { }"
    kom_create_person "QersoN 8" "" "00000000" "0 { }"
    kom_create_person "CONFERENCE 9" "" "00000000" "0 { }"
    kom_create_person "conference 10" "" "00000000" "0 { }"
    kom_create_person "ConferencE 11" "" "00000000" "0 { }"

    kom_create_conference "CONFERENCE 12" "00000000" "0 { }"
    kom_create_conference "conference 13" "00000000" "0 { }"
    kom_create_conference "ConferencE 14" "00000000" "0 { }"
    kom_create_conference "QERSON 15" "00000000" "0 { }"
    kom_create_conference "qerson 16" "00000000" "0 { }"
    kom_create_conference "QersoN 17" "00000000" "0 { }"
}

proc shutdown {{expected_leaks {}}} {
    kom_logout
    kom_login 5 "gazonk" 0
    kom_enable 255

    send "9999 44 0\n"
    simple_expect "=9999"
    client_death 0
    lyskomd_death $expected_leaks
}


startup

send "1000 74 [holl "Q.*N"] 0 0\n"
simple_expect "=1000 0 \\*"

send "1001 74 [holl "Q.*N"] 0 1\n"
simple_expect "=1001 2 { [holl "QERSON 15"] 0000 15 [holl "QersoN 17"] 0000 17 }"

send "1002 74 [holl "Q.*N"] 1 0\n"
simple_expect "=1002 2 { [holl "QERSON 6"] 1001 6 [holl "QersoN 8"] 1001 8 }"

send "1003 74 [holl "Q.*N"] 1 1\n"
simple_expect "=1003 4 { [holl "QERSON 6"] 1001 6 [holl "QersoN 8"] 1001 8 [holl "QERSON 15"] 0000 15 [holl "QersoN 17"] 0000 17 }"

shutdown


startup

send "2000 74 [holl "Q.*N"] 0 0\n"
simple_expect "=2000 0 \\*"

send "2001 74 [holl "Q.*N"] 0 1\n"
simple_expect "=2001 2 { [holl "QERSON 15"] 0000 15 [holl "QersoN 17"] 0000 17 }"

send "2002 74 [holl "Q.*N"] 1 0\n"
simple_expect "=2002 2 { [holl "QERSON 6"] 1001 6 [holl "QersoN 8"] 1001 8 }"

send "2003 74 [holl "Q.*N"] 1 1\n"
simple_expect "=2003 4 { [holl "QERSON 6"] 1001 6 [holl "QersoN 8"] 1001 8 [holl "QERSON 15"] 0000 15 [holl "QersoN 17"] 0000 17 }"

send "3001 74 [holl "Error\["] 0 0\n"
simple_expect "%3001 43 0"

shutdown
