# Test suite for lyskomd.
# Copyright (C) 1999, 2001, 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test that the correct persons can change between
# recpt, cc-recpt and bcc-recpt.

# Start a server and set up a minmal environment:
#   person 6     "Author"
#   text 1       Written by author, conference 1 as recipient.

lyskomd_start

client_start 0

talk_to client 0
send "A7Hfoo bar\n"
simple_expect "LysKOM" "connected"


send "1000 89 [holl "Author"] [holl "Writer"] 00000000 0 { }\n"
simple_expect "=1000 6"

send "1001 62 6 [holl "Writer"] 1\n"
simple_expect "=1001"

send "1002 86 [holl "Text 1"] 1 { 0 1 } 0 { }\n"
simple_expect "=1002 1"

send "1003 86 [holl "Text 2"] 1 { 1 1 } 0 { }\n"
simple_expect "=1003 2"

send "1004 86 [holl "Text 3"] 1 { 15 1 } 0 { }\n"
simple_expect "=1004 3"

send "1005 30 1 1 1\n"
simple_expect "=1005"

send "1006 30 2 1 15\n"
simple_expect "=1006"

send "1007 30 3 1 0\n"
simple_expect "=1007"

send "1008 1\n"
simple_expect "=1008"

# Create a new person and attempt to move the texts some more.
send "1009 89 [holl "Mover"] [holl "mwm"] 00000000 0 { }\n"
simple_expect "=1009 7"

send "1010 62 7 [holl "mwm"] 1\n"
simple_expect "=1010"

send "1011 30 1 1 15\n"
simple_expect "%1011 12 1"

send "1012 30 2 1 0\n"
simple_expect "%1012 12 1"

send "1013 30 3 1 1\n"
simple_expect "%1013 12 1"

# Add one of the texts to a new conference, and then try to change the type.
send "1014 30 1 2 0\n"
simple_expect "=1014"

send "1015 30 1 2 1\n"
good_bad_expect "=1015" "%12 2"

send "1016 62 5 [holl "gazonk"] 1\n"
simple_expect "=1016"

send "1017 42 255\n"
simple_expect "=1017"

# Attempt to add a broken FAQ aux-item with trailing garbage.
send "1018 93 4 0 { } 1 { 14 00000000 0 [holl "3garbage"] }\n"
simple_expect "%1018 64 0"

# Create a proper FAQ item
send "1019 93 4 0 { } 1 { 14 00000000 0 [holl "3"] }\n"
simple_expect "=1019"

# Create another proper FAQ item
send "1020 93 2 0 { } 1 { 14 00000000 0 [holl "1"] }\n"
simple_expect "=1020"

send "1021 91 4\n"
simple_expect "=1021 [holl "Nyheter om LysKOM"] 00001000 $any_time $any_time 0 0 0 0 0 0 77 77 0 1 0 0 1 { 1 14 5 $any_time 00000000 1 [holl "3"] }"

send "1022 90 3\n"
simple_expect "=1022 $any_time 6 0 6 0 2 { 0 1 6 3 } 1 { 1 28 5 $any_time 00001000 0 [holl "4"] }"

send "1023 44 0\n"
simple_expect "=1023"

client_death 0

lyskomd_death
