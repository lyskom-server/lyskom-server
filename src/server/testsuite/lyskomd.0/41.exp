# Test suite for lyskomd.
# Copyright (C) 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test async-new-motd.
# Also test first-unused-conf-no and first-unused-text-no.
# Also test find-next-conf-no and find-previous-conf-no.
#
# Persons:
#     5: admin
#     6: super: creator of 9 and 10
#     7: member: a member of 9 and 10
#     8: bystander: not a member
#
# Conferences:
#     9: public
#    10: secret
#
# Texts:
#     1: conf 1 as recipient
#     2: conf 1 as recipient
#     3: conf 10 as recipient
#     4: conf 10 as recipient

lyskomd_start

# Log in as admin, and enable.
client_start 5
send "A[holl "five"]\n"
simple_expect "LysKOM" "connected"
# 114:first-unused-conf-no
send "1000 114\n"
simple_expect "=1000 6"
# 115:first-unused-text-no
send "1001 115\n"
simple_expect "=1001 1"
send "1002 80 1 { 21 }\n"
simple_expect "=1002"
send "1003 0 5 [holl "gazonk"]\n"
simple_expect "=1003"
send "1004 42 255\n"
simple_expect "=1004"

# Create person 6 and log in.
client_start 6
send "A[holl "six"]\n"
simple_expect "LysKOM" "connected"
send "1005 80 1 { 21 }\n"
simple_expect "=1005"
send "1006 89 [holl "super"] [holl "super"] 00000000 0 { }\n"
simple_expect "=1006 6"
send "1007 0 6 [holl "super"]\n"
simple_expect "=1007"

# Create person 7 and log in.
client_start 7
send "A[holl "seven"]\n"
simple_expect "LysKOM" "connected"
send "1008 80 1 { 21 }\n"
simple_expect "=1008"
send "1009 89 [holl "member"] [holl "member"] 00000000 0 { }\n"
simple_expect "=1009 7"
send "1010 0 7 [holl "member"]\n"
simple_expect "=1010"

# Create person 8 and log in.
client_start 8
send "A[holl "eight"]\n"
simple_expect "LysKOM" "connected"
send "1011 80 1 { 21 }\n"
simple_expect "=1011"
send "1012 89 [holl "bystander"] [holl "bystander"] 00000000 0 { }\n"
simple_expect "=1012 8"
send "1013 0 8 [holl "bystander"]\n"
simple_expect "=1013"

# Create conferences 9 and 10, and let person 6 and 7 join them.
talk_to client 6
send "1014 88 [holl "public"] 00000000 0 { }\n"
simple_expect "=1014 9"
send "1015 88 [holl "secret"] 10100000 0 { }\n"
simple_expect "=1015 10"
send "1016 100 9 6 200 1 00000000\n"
simple_expect "=1016"
send "1017 100 9 7 200 1 00000000\n"
lyskomd_expect "Person 7 added to conference 9 by 6\."
simple_expect "=1017"
send "1018 100 10 6 200 1 00000000\n"
simple_expect "=1018"
send "1019 100 10 7 200 1 00000000\n"
lyskomd_expect "Person 7 added to conference 10 by 6\."
simple_expect "=1019"

# Person 7 accepts the invitations.
talk_to client 7
send "1020 100 9 7 200 1 00000000\n"
simple_expect "=1020"
send "1021 100 10 7 200 1 00000000\n"
simple_expect "=1021"

# Person 6 creates the four texts that will be used as motds.
talk_to client 6
send "1022 86 [holl "text 1"] 1 { 0 1 } 0 { }\n"
simple_expect "=1022 1"
send "1023 86 [holl "text 2"] 1 { 0 1 } 0 { }\n"
simple_expect "=1023 2"
send "1024 86 [holl "text 3"] 1 { 0 10 } 0 { }\n"
simple_expect "=1024 3"
send "1025 86 [holl "text 4"] 1 { 0 10 } 0 { }\n"
simple_expect "=1025 4"

# Set text 1 as motd of conference 9.
send "1026 17 9 1\n"
simple_expect ":3 21 9 0 1"
client_expect 5 ":3 21 9 0 1"
client_expect 7 ":3 21 9 0 1"
client_expect 8 ":3 21 9 0 1"
simple_expect "=1026"

# Set text 2 as motd of conference 9.
send "1027 17 9 2\n"
simple_expect ":3 21 9 1 2"
client_expect 5 ":3 21 9 1 2"
client_expect 7 ":3 21 9 1 2"
client_expect 8 ":3 21 9 1 2"
simple_expect "=1027"

# Set text 3 as motd of conference 9.  Since it isn't visible
# to the bystander, the bystander will think the motd has been
# removed.
send "1028 17 9 3\n"
simple_expect ":3 21 9 2 3"
client_extracting_expect 5 ":3 21 9 2 (0|3)" seen 1
set test "administrator sees proper text"
if {$seen == 3} {
    pass "$test"
} else {
    fail "$test (filtered)"
}
unset test
client_expect 7 ":3 21 9 2 3"
client_expect 8 ":3 21 9 2 0"
simple_expect "=1028"

# Set text 4 as presentation of conference 9.  This text is also not
# visible to the bystander.  In the view of the bystander, the
# presentation changes from 0 to 0.  That is no change, so no async
# message is sent to the bystander.
send "1029 17 9 4\n"
simple_expect ":3 21 9 3 4"

client_expect 5 ":3 21 9 3 4"

client_expect 7 ":3 21 9 3 4"
simple_expect "=1029"

# View the conf-stat of conference 9.
send "1030 91 9\n"
simple_expect "=1030 [holl "public"] 00000000 $any_time $any_time 6 0 6 0 6 4 77 77 2 1 0 0 0 \\*"
talk_to client 5
send "1031 91 9\n"
simple_expect "=1031 [holl "public"] 00000000 $any_time $any_time 6 0 6 0 6 4 77 77 2 1 0 0 0 \\*"
talk_to client 7
send "1032 91 9\n"
simple_expect "=1032 [holl "public"] 00000000 $any_time $any_time 6 0 6 0 6 4 77 77 2 1 0 0 0 \\*"

talk_to client 8
send "1033 91 9\n"
extracting_expect "=1033 [holl "public"] 00000000 $any_time $any_time 6 0 6 0 6 ($any_num) 77 77 2 1 0 0 0 \\*" motd 1
setup_xfail "*-*-*" "Bug 1074"
set test "Unreadable motd filtered"
if {$motd == 0} {
    pass "$test"
} else {
    fail "$test (motd visible)"
    if {$motd != 4} {
	fail "$test (strange result $motd)"
    }
}
unset test

# Set text 1 as motd of conference 10.
talk_to client 6
send "1034 17 10 1\n"
simple_expect ":3 21 10 0 1"
client_expect 5 ":3 21 10 0 1"
client_expect 7 ":3 21 10 0 1"
simple_expect "=1034"

# Set text 2 as motd of conference 10.
send "1035 17 10 2\n"
simple_expect ":3 21 10 1 2"
client_expect 5 ":3 21 10 1 2"
client_expect 7 ":3 21 10 1 2"
simple_expect "=1035"

# Set text 3 as motd of conference 10.
# removed.
send "1036 17 10 3\n"
simple_expect ":3 21 10 2 3"
client_extracting_expect 5 ":3 21 10 2 (0|3)" seen 1
set test "administrator sees proper text"
if {$seen == 3} {
    pass "$test"
} else {
    fail "$test (filtered)"
}
unset test
client_expect 7 ":3 21 10 2 3"
simple_expect "=1036"

# Set text 4 as motd of conference 10.
send "1037 17 10 4\n"
simple_expect ":3 21 10 3 4"

client_expect 5 ":3 21 10 3 4"

client_expect 7 ":3 21 10 3 4"
simple_expect "=1037"

# View the conf-stat of conference 10.
send "1038 91 10\n"
simple_expect "=1038 [holl "secret"] 10100000 $any_time $any_time 6 0 6 0 6 4 77 77 2 1 2 0 0 \\*"
talk_to client 5
send "1039 91 10\n"
simple_expect "=1039 [holl "secret"] 10100000 $any_time $any_time 6 0 6 0 6 4 77 77 2 1 2 0 0 \\*"
talk_to client 7
send "1040 91 10\n"
simple_expect "=1040 [holl "secret"] 10100000 $any_time $any_time 6 0 6 0 6 4 77 77 2 1 2 0 0 \\*"

talk_to client 8
send "1041 91 10\n"
simple_expect "%1041 9 10"

# Test find-next-conf-no and find-previous-conf-no.

# Person 5 is allowed to see everything.
talk_to client 5

send "1042 116 0\n"
simple_expect "=1042 1"
send "1043 116 1\n"
simple_expect "=1043 2"
send "1044 116 2\n"
simple_expect "=1044 3"
send "1045 116 3\n"
simple_expect "=1045 4"
send "1046 116 4\n"
simple_expect "=1046 5"
send "1047 116 5\n"
simple_expect "=1047 6"
send "1048 116 6\n"
simple_expect "=1048 7"
send "1049 116 7\n"
simple_expect "=1049 8"
send "1050 116 8\n"
simple_expect "=1050 9"
send "1051 116 9\n"
simple_expect "=1051 10"
send "1052 116 10\n"
simple_expect "%1052 9 10"
send "1053 116 11\n"
simple_expect "%1053 9 11"

send "1054 117 0\n"
simple_expect "%1054 9 0"
send "1055 117 1\n"
simple_expect "%1055 9 1"
send "1056 117 2\n"
simple_expect "=1056 1"
send "1057 117 3\n"
simple_expect "=1057 2"
send "1058 117 4\n"
simple_expect "=1058 3"
send "1059 117 5\n"
simple_expect "=1059 4"
send "1060 117 6\n"
simple_expect "=1060 5"
send "1061 117 7\n"
simple_expect "=1061 6"
send "1062 117 8\n"
simple_expect "=1062 7"
send "1063 117 9\n"
simple_expect "=1063 8"
send "1064 117 10\n"
simple_expect "=1064 9"
send "1065 117 11\n"
simple_expect "=1065 10"
send "1066 117 12\n"
simple_expect "=1066 10"
send "1067 117 120\n"
simple_expect "=1067 10"

# Person 6 is allowed to see everything.
talk_to client 6

send "1068 116 0\n"
simple_expect "=1068 1"
send "1069 116 1\n"
simple_expect "=1069 2"
send "1070 116 2\n"
simple_expect "=1070 3"
send "1071 116 3\n"
simple_expect "=1071 4"
send "1072 116 4\n"
simple_expect "=1072 5"
send "1073 116 5\n"
simple_expect "=1073 6"
send "1074 116 6\n"
simple_expect "=1074 7"
send "1075 116 7\n"
simple_expect "=1075 8"
send "1076 116 8\n"
simple_expect "=1076 9"
send "1077 116 9\n"
simple_expect "=1077 10"
send "1078 116 10\n"
simple_expect "%1078 9 10"
send "1079 116 11\n"
simple_expect "%1079 9 11"

send "1080 117 0\n"
simple_expect "%1080 9 0"
send "1081 117 1\n"
simple_expect "%1081 9 1"
send "1082 117 2\n"
simple_expect "=1082 1"
send "1083 117 3\n"
simple_expect "=1083 2"
send "1084 117 4\n"
simple_expect "=1084 3"
send "1085 117 5\n"
simple_expect "=1085 4"
send "1086 117 6\n"
simple_expect "=1086 5"
send "1087 117 7\n"
simple_expect "=1087 6"
send "1088 117 8\n"
simple_expect "=1088 7"
send "1089 117 9\n"
simple_expect "=1089 8"
send "1090 117 10\n"
simple_expect "=1090 9"
send "1091 117 11\n"
simple_expect "=1091 10"
send "1092 117 12\n"
simple_expect "=1092 10"
send "1093 117 120\n"
simple_expect "=1093 10"

# Person 7 is allowed to see everything.
talk_to client 7

send "1094 116 0\n"
simple_expect "=1094 1"
send "1095 116 1\n"
simple_expect "=1095 2"
send "1096 116 2\n"
simple_expect "=1096 3"
send "1097 116 3\n"
simple_expect "=1097 4"
send "1098 116 4\n"
simple_expect "=1098 5"
send "1099 116 5\n"
simple_expect "=1099 6"
send "1100 116 6\n"
simple_expect "=1100 7"
send "1101 116 7\n"
simple_expect "=1101 8"
send "1102 116 8\n"
simple_expect "=1102 9"
send "1103 116 9\n"
simple_expect "=1103 10"
send "1104 116 10\n"
simple_expect "%1104 9 10"
send "1105 116 11\n"
simple_expect "%1105 9 11"

send "1106 117 0\n"
simple_expect "%1106 9 0"
send "1107 117 1\n"
simple_expect "%1107 9 1"
send "1108 117 2\n"
simple_expect "=1108 1"
send "1109 117 3\n"
simple_expect "=1109 2"
send "1110 117 4\n"
simple_expect "=1110 3"
send "1111 117 5\n"
simple_expect "=1111 4"
send "1112 117 6\n"
simple_expect "=1112 5"
send "1113 117 7\n"
simple_expect "=1113 6"
send "1114 117 8\n"
simple_expect "=1114 7"
send "1115 117 9\n"
simple_expect "=1115 8"
send "1116 117 10\n"
simple_expect "=1116 9"
send "1117 117 11\n"
simple_expect "=1117 10"
send "1118 117 12\n"
simple_expect "=1118 10"
send "1119 117 120\n"
simple_expect "=1119 10"

# Person 8 is not allowed to see the secret conference 10
talk_to client 8

send "1120 116 0\n"
simple_expect "=1120 1"
send "1121 116 1\n"
simple_expect "=1121 2"
send "1122 116 2\n"
simple_expect "=1122 3"
send "1123 116 3\n"
simple_expect "=1123 4"
send "1124 116 4\n"
simple_expect "=1124 5"
send "1125 116 5\n"
simple_expect "=1125 6"
send "1126 116 6\n"
simple_expect "=1126 7"
send "1127 116 7\n"
simple_expect "=1127 8"
send "1128 116 8\n"
simple_expect "=1128 9"
send "1129 116 9\n"
simple_expect "%1129 9 9"
send "1130 116 10\n"
simple_expect "%1130 9 10"
send "1131 116 11\n"
simple_expect "%1131 9 11"

send "1132 117 0\n"
simple_expect "%1132 9 0"
send "1133 117 1\n"
simple_expect "%1133 9 1"
send "1134 117 2\n"
simple_expect "=1134 1"
send "1135 117 3\n"
simple_expect "=1135 2"
send "1136 117 4\n"
simple_expect "=1136 3"
send "1137 117 5\n"
simple_expect "=1137 4"
send "1138 117 6\n"
simple_expect "=1138 5"
send "1139 117 7\n"
simple_expect "=1139 6"
send "1140 117 8\n"
simple_expect "=1140 7"
send "1141 117 9\n"
simple_expect "=1141 8"
send "1142 117 10\n"
simple_expect "=1142 9"
send "1143 117 11\n"
simple_expect "=1143 9"
send "1144 117 12\n"
simple_expect "=1144 9"
send "1145 117 120\n"
simple_expect "=1145 9"


# Shut down.
talk_to client 5
# 114:first-unused-conf-no
send "1146 114\n"
simple_expect "=1146 11"
# 115:first-unused-text-no
send "1147 115\n"
simple_expect "=1147 5"
send "1148 44 0\n"
simple_expect "=1148"
client_death 5
client_death 6
client_death 7
client_death 8

lyskomd_death

