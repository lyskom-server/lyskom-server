# Test suite for lyskomd.
# Copyright (C) 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test async-new-presentation.
#
# Persons:
#     5: admin
#     6: super: creator of 9 and 10
#     7: member: a member of 9 and 10
#     8: bystander: not a member
#
# Conferences:
#     9: public
#    10: secret
#
# Texts:
#     1: conf 1 as recipient
#     2: conf 1 as recipient
#     3: conf 10 as recipient
#     4: conf 10 as recipient

lyskomd_start

# Log in as admin, and enable.
client_start 5
send "A[holl "five"]\n"
simple_expect "LysKOM" "connected"
send "1000 80 1 { 20 }\n"
good_bad_expect "=1000" "%50 20"
send "1001 0 5 [holl "gazonk"]\n"
simple_expect "=1001"
send "1002 42 255\n"
simple_expect "=1002"

# Create person 6 and log in.
client_start 6
send "A[holl "six"]\n"
simple_expect "LysKOM" "connected"
send "1003 80 1 { 20 }\n"
good_bad_expect "=1003" "%50 20"
send "1004 89 [holl "super"] [holl "super"] 00000000 0 { }\n"
simple_expect "=1004 6"
send "1005 0 6 [holl "super"]\n"
simple_expect "=1005"

# Create person 7 and log in.
client_start 7
send "A[holl "seven"]\n"
simple_expect "LysKOM" "connected"
send "1006 80 1 { 20 }\n"
good_bad_expect "=1006" "%50 20"
send "1007 89 [holl "member"] [holl "member"] 00000000 0 { }\n"
simple_expect "=1007 7"
send "1008 0 7 [holl "member"]\n"
simple_expect "=1008"

# Create person 8 and log in.
client_start 8
send "A[holl "eight"]\n"
simple_expect "LysKOM" "connected"
send "1009 80 1 { 20 }\n"
good_bad_expect "=1009" "%50 20"
send "1010 89 [holl "bystander"] [holl "bystander"] 00000000 0 { }\n"
simple_expect "=1010 8"
send "1011 0 8 [holl "bystander"]\n"
simple_expect "=1011"

# Create conferences 9 and 10, and let person 6 and 7 join them.
talk_to client 6
send "1012 88 [holl "public"] 00000000 0 { }\n"
simple_expect "=1012 9"
send "1013 88 [holl "secret"] 10100000 0 { }\n"
simple_expect "=1013 10"
send "1014 100 9 6 200 1 00000000\n"
simple_expect "=1014"
send "1015 100 9 7 200 1 00000000\n"
lyskomd_expect "Person 7 added to conference 9 by 6\."
simple_expect "=1015"
send "1016 100 10 6 200 1 00000000\n"
simple_expect "=1016"
send "1017 100 10 7 200 1 00000000\n"
lyskomd_expect "Person 7 added to conference 10 by 6\."
simple_expect "=1017"

# Person 7 accepts the invitations.
talk_to client 7
send "1018 100 9 7 200 1 00000000\n"
simple_expect "=1018"
send "1019 100 10 7 200 1 00000000\n"
simple_expect "=1019"

# Person 6 creates the four texts that will be used as presentations.
talk_to client 6
send "1020 86 [holl "text 1"] 1 { 0 1 } 0 { }\n"
simple_expect "=1020 1"
send "1021 86 [holl "text 2"] 1 { 0 1 } 0 { }\n"
simple_expect "=1021 2"
send "1022 86 [holl "text 3"] 1 { 0 10 } 0 { }\n"
simple_expect "=1022 3"
send "1023 86 [holl "text 4"] 1 { 0 10 } 0 { }\n"
simple_expect "=1023 4"

# Set text 1 as presentation of conference 9.
send "1024 16 9 1\n"
simple_expect ":3 20 9 0 1"
client_expect 5 ":3 20 9 0 1"
client_expect 7 ":3 20 9 0 1"
client_expect 8 ":3 20 9 0 1"
simple_expect "=1024"

# Set text 2 as presentation of conference 9.
send "1025 16 9 2\n"
simple_expect ":3 20 9 1 2"
client_expect 5 ":3 20 9 1 2"
client_expect 7 ":3 20 9 1 2"
client_expect 8 ":3 20 9 1 2"
simple_expect "=1025"

# Set text 3 as presentation of conference 9.  Since it isn't visible
# to the bystander, the bystander will think the presentation has been
# removed.
send "1026 16 9 3\n"
simple_expect ":3 20 9 2 3"
client_extracting_expect 5 ":3 20 9 2 (0|3)" seen 1
set test "administrator sees proper text"
if {$seen == 3} {
    pass "$test"
} else {
    fail "$test (filtered)"
}
unset test
client_expect 7 ":3 20 9 2 3"
client_expect 8 ":3 20 9 2 0"
simple_expect "=1026"

# Set text 4 as presentation of conference 9.  This text is also not
# visible to the bystander.  In the view of the bystander, the
# presentation changes from 0 to 0.  That is no change, so no async
# message is sent to the bystander.
send "1027 16 9 4\n"
simple_expect ":3 20 9 3 4"

client_expect 5 ":3 20 9 3 4"

client_expect 7 ":3 20 9 3 4"
simple_expect "=1027"

# View the conf-stat of conference 9.
send "1028 91 9\n"
simple_expect "=1028 [holl "public"] 00000000 $any_time $any_time 6 4 6 0 6 0 77 77 2 1 0 0 0 \\*"
talk_to client 5
send "1029 91 9\n"
simple_expect "=1029 [holl "public"] 00000000 $any_time $any_time 6 4 6 0 6 0 77 77 2 1 0 0 0 \\*"
talk_to client 7
send "1030 91 9\n"
simple_expect "=1030 [holl "public"] 00000000 $any_time $any_time 6 4 6 0 6 0 77 77 2 1 0 0 0 \\*"

talk_to client 8
send "1031 91 9\n"
extracting_expect "=1031 [holl "public"] 00000000 $any_time $any_time 6 ($any_num) 6 0 6 0 77 77 2 1 0 0 0 \\*" pres 1
setup_xfail "*-*-*" "Bug 1074"
set test "Unreadable presentation filtered"
if {$pres == 0} {
    pass "$test"
} else {
    fail "$test (presentation visible)"
    if {$pres != 4} {
	fail "$test (strange result $pres)"
    }
}
unset test

# Set text 1 as presentation of conference 10.
talk_to client 6
send "1032 16 10 1\n"
simple_expect ":3 20 10 0 1"
client_expect 5 ":3 20 10 0 1"
client_expect 7 ":3 20 10 0 1"
simple_expect "=1032"

# Set text 2 as presentation of conference 10.
send "1033 16 10 2\n"
simple_expect ":3 20 10 1 2"
client_expect 5 ":3 20 10 1 2"
client_expect 7 ":3 20 10 1 2"
simple_expect "=1033"

# Set text 3 as presentation of conference 10.
# removed.
send "1034 16 10 3\n"
simple_expect ":3 20 10 2 3"
client_extracting_expect 5 ":3 20 10 2 (0|3)" seen 1
set test "administrator sees proper text"
if {$seen == 3} {
    pass "$test"
} else {
    fail "$test (filtered)"
}
unset test
client_expect 7 ":3 20 10 2 3"
simple_expect "=1034"

# Set text 4 as presentation of conference 10.
send "1035 16 10 4\n"
simple_expect ":3 20 10 3 4"

client_expect 5 ":3 20 10 3 4"

client_expect 7 ":3 20 10 3 4"
simple_expect "=1035"

# View the conf-stat of conference 10.
send "1036 91 10\n"
simple_expect "=1036 [holl "secret"] 10100000 $any_time $any_time 6 4 6 0 6 0 77 77 2 1 2 0 0 \\*"
talk_to client 5
send "1037 91 10\n"
simple_expect "=1037 [holl "secret"] 10100000 $any_time $any_time 6 4 6 0 6 0 77 77 2 1 2 0 0 \\*"
talk_to client 7
send "1038 91 10\n"
simple_expect "=1038 [holl "secret"] 10100000 $any_time $any_time 6 4 6 0 6 0 77 77 2 1 2 0 0 \\*"

talk_to client 8
send "1039 91 10\n"
simple_expect "%1039 9 10"

# Shut down.
talk_to client 5
send "1040 44 0\n"
simple_expect "=1040"
client_death 5
client_death 6
client_death 7
client_death 8

lyskomd_death

