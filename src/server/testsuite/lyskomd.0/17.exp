# Test suite for lyskomd.
# Copyright (C) 1999-2000, 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Check the recommended-conf aux-item, especially the validate regexp.

read_versions

lyskomd_start

client_start 0
talk_to client 0
send "A3Hfoo\n"
simple_expect "LysKOM" "connected"
send "1000 62 5 [holl "gazonk"] 1\n"
simple_expect "=1000"

send "1001 94\n"
simple_expect "=1001 $server_compat_version 1 2 3 4 0 0 \\*"

# We cannot modify the system info unless we enable our privileges.
send "1002 95 0 { } 1 { 29 00000000 1 [holl "1"] }\n"
simple_expect "%1002 12 0"
send "1003 42 255\n"
simple_expect "=1003"

# Three valid items.
send "1004 95 0 { } 1 { 29 00000000 1 [holl "1"] }\n"
simple_expect "=1004"

send "1005 94\n"
simple_expect "=1005 $server_compat_version 1 2 3 4 0 1 { 1 29 5 $any_time 00000000 1 [holl "1"] }"

send "1006 95 0 { } 1 { 29 00000000 1 [holl "2 250"] }\n"
simple_expect "=1006"

send "1007 95 0 { } 1 { 29 00000000 1 [holl "3 250 00000000"] }\n"
simple_expect "=1007"

# A few invalid values.
send "1008 95 0 { } 1 { 29 00000000 1 [holl "4 250 00000000 4"] }\n"
simple_expect "%1008 64 0"

send "1009 95 0 { } 1 { 29 00000000 1 [holl "4  00000000"] }\n"
simple_expect "%1009 64 0"

send "1010 95 0 { } 1 { 29 00000000 1 [holl "4 2c0 00000000"] }\n"
simple_expect "%1010 64 0"

send "1011 95 0 { } 1 { 29 00000000 1 [holl "4 250 00200000"] }\n"
simple_expect "%1011 64 0"

# A fourth valid item.
send "1012 95 0 { } 1 { 29 00000000 1 [holl "4 190 00000000"] }\n"
simple_expect "=1012"

send "1005 94\n"
simple_expect "=1005 $server_compat_version 1 2 3 4 0 4 { 1 29 5 $any_time 00000000 1 [holl "1"] 2 29 5 $any_time 00000000 1 [holl "2 250"] 3 29 5 $any_time 00000000 1 [holl "3 250 00000000"] 4 29 5 $any_time 00000000 1 [holl "4 190 00000000"] }"

send "1013 44 0\n"
simple_expect "=1013"
client_death 0

lyskomd_death
