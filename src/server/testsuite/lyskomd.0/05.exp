# Test suite for lyskomd.
# Copyright (C) 1998-1999, 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 


# Test of membership functionality
#
# Preconditions:
#   Person 5 exists, can enable to 255 and has password "gazonk"
#

read_versions
source "$srcdir/config/prot-a.exp"


# ----------------------------------------------------------------------
# startup_05 sets up the database and starts client 0
#
# The sample database
#
# Person 6 "P6"; supervisor 6; password pw1
# Person 7 "P7"; supervisor 6; password pw2
# Person 8 "P8"; supervisor 7; password pw3
# Person 13 "P13"; supervisor 8; password pw4
# Person 14 "P14"; supervisor 14; password pw5
# Set supervisor of P8 to P14
#
# Conference 9 "C9"; created by 7; rd_prot 
# Conference 10 "C10"; created by 7; secret
# Conference 11 "C11"; created by 7; forbid_secret
# Conference 12 "C12"; created by 7; no flags
# ----------------------------------------------------------------------
    
proc startup_05 {} {
    client_start 0
    talk_to client 0
    send "A[holl "DejaGnu test suite"]\n"
    simple_expect "LysKOM" "MSHIP: Connected"

    kom_accept_async "0 { }" "" "MSHIP: accept-async"

    kom_create_person "P6" "pw1" "00000000" "0 { }"
    kom_login 6 "pw1" 0
    kom_create_person "P7" "pw2" "00000000" "0 { }"
    kom_logout
    kom_create_person "P8" "pw3" "00000000" "0 { }"
    kom_logout

    kom_login 7 "pw2" 0
    
    kom_create_conference "C9"  "10000000" "0 { }"
    kom_create_conference "C10" "10100000" "0 { }"
    kom_create_conference "C11" "00000100" "0 { }"
    kom_create_conference "C12" "00000000" "0 { }"
    
    kom_logout
    kom_login 8 "pw3" 0
    kom_create_person "P13" "pw4" "00000000" "0 { }"

    kom_logout
    kom_create_person "P14" "pw5" "00000000" "0 { }"
    
    kom_logout
    kom_login 5 "gazonk" 0
    kom_enable 255
    kom_set_supervisor 8 14
}

proc shutdown_05 {} {
    kom_logout
    kom_login 5 "gazonk" 0
    kom_enable 255

    send "9999 44 0\n"
    simple_expect "=9999"
    client_death 0
    lyskomd_death
}



lyskomd_start
startup_05


# ----------------------------------------------------------------------
# Basic tests
#
# For each test perform the function then get the membership list for
# the affected person and then the member list for the affected
# conference.
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# 1. Person 7 becoming member of conference 9.

kom_logout
kom_login 7 "pw2" 0

send "1000 100 9 7 200 0 00000000\n"
simple_expect "=1000"

send "1001 101 9 0 10000\n"
simple_expect "=1001 1 { 7 7 $any_time 00000000 }"

send "1002 99 7 0 10000 0\n"
simple_expect "=1002 2 { 0 $any_time 9 200 0 0 \\* 7 $any_time 00000000 1 $any_time 7 255 0 0 \\* 6 $any_time 00000000 }"


# ----------------------------------------------------------------------
# 2. Person 7 adding person 8 as member of conference 9 as invitation 

kom_logout
kom_login 7 "pw2" 0

send "1010 100 9 8 200 0 10000000\n"
lyskomd_expect "Person 8 added to conference 9 by 7."
simple_expect "=1010"

send "1011 101 9 0 10000\n"
simple_expect "=1011 2 { 7 7 $any_time 00000000 8 7 $any_time 10000000 }"

send "1012 99 8 0 10000 0\n"
simple_expect "=1012 2 { 0 $any_time 9 200 0 0 \\* 7 $any_time 10000000 1 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"


# ----------------------------------------------------------------------
# 3. Person 8 changing type of membership to 00100000 (secret)

kom_logout
kom_login 8 "pw3" 0

send "1020 102 8 9 00100000\n"
simple_expect "=1020"

send "1021 101 9 0 10000\n"
simple_expect "=1021 2 { 7 7 $any_time 00000000 8 7 $any_time 00100000 }"

send "1022 99 8 0 10000 0\n"
simple_expect "=1022 2 { 0 $any_time 9 200 0 0 \\* 7 $any_time 00100000 1 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"

# ----------------------------------------------------------------------
# Try to convert conference 8 to forbid-secret. This should fail.

kom_logout
kom_login 7 "pw2" 0

send "1023 21 9 00000100\n"
simple_expect "%1023 54 8"


# ----------------------------------------------------------------------
# 4. Person 7 looking at membership of conference 9
#    Person 7 looking at members of conference 9
#       Membership of person 8 is not blanked out since person 7 is
#       organizer of conference 9

kom_logout
kom_login 7 "pw2" 0

send "1030 101 9 0 10000\n"
simple_expect "=1030 2 { 7 7 $any_time 00000000 8 7 $any_time 00100000 }"

send "1031 99 8 0 10000 0\n"
simple_expect "=1031 2 { 0 $any_time 9 200 0 0 \\* 7 $any_time 00100000 1 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"


# ----------------------------------------------------------------------
# 5. Person 13 looking at members of conference 9
#       Membership of person 8 is blanked out in get-members
#    Person 13 looking at membership of person 8
#       Membership of person 8 is removed in get-membership    

kom_logout
kom_login 13 "pw4" 0

send "1040 101 9 0 10000\n"
simple_expect "=1040 2 { 7 7 $any_time 00000000 0 0 $epoch_time 00100000 }"

send "1041 99 8 0 10000 0\n"
simple_expect "=1041 1 { 0 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"

# ----------------------------------------------------------------------
# 6. Admin looking at members of conference 9
#    Admin looking at membership of person 8

kom_logout
kom_login 5 "gazonk" 0

send "1050 101 9 0 10000\n"
simple_expect "=1050 2 { 7 7 $any_time 00000000 0 0 $epoch_time 00100000 }"

send "1051 99 8 0 10000 0\n"
simple_expect "=1051 1 { 0 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"


kom_enable 255

send "1052 101 9 0 10000\n"
simple_expect "=1052 2 { 7 7 $any_time 00000000 8 7 $any_time 00100000 }"

send "1053 99 8 0 10000 0\n"
simple_expect "=1053 2 { 0 $any_time 9 200 0 0 \\* 7 $any_time 00100000 1 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"


# ----------------------------------------------------------------------
# 7. Supervisor of P8 looking at membership of person 8
#    Supervisor of P8 looking at members of conference 9
#       Secret memberships should be visible

kom_logout
kom_login 14 "pw5" 0

send "1060 101 9 0 10000\n"
simple_expect "=1060 2 { 7 7 $any_time 00000000 8 7 $any_time 00100000 }"

send "1061 99 8 0 10000 0\n"
simple_expect "=1061 2 { 0 $any_time 9 200 0 0 \\* 7 $any_time 00100000 1 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"


# ----------------------------------------------------------------------
# 8. Test that we can't become a secret member of a forbid-secret conf
#    Test that we can't set mship-type to secret in a forbid-secret conf

kom_logout
kom_login 8 "pw3" 0

send "1070 100 11 8 201 0 00100000\n"
simple_expect "%1070 54 0"

send "1071 99 8 0 10000 0\n"
simple_expect "=1071 2 { 0 $any_time 9 200 0 0 \\* 7 $any_time 00100000 1 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"

send "1072 101 11 0 10000\n"
simple_expect "%1072 19 0"


# Become member

send "1073 100 11 8 201 0 00000000\n"
simple_expect "=1073"

send "1074 99 8 0 10000 0\n"
simple_expect "=1074 3 { 0 $any_time 11 201 0 0 \\* 8 $any_time 00000000 1 $any_time 9 200 0 0 \\* 7 $any_time 00100000 2 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"

send "1075 101 11 0 10000\n"
simple_expect "=1075 1 { 8 8 $any_time 00000000 }"

# Change type

send "1076 102 8 11 00100000\n"
simple_expect "%1076 54 0"

send "1077 99 8 0 10000 0\n"
simple_expect "=1077 3 { 0 $any_time 11 201 0 0 \\* 8 $any_time 00000000 1 $any_time 9 200 0 0 \\* 7 $any_time 00100000 2 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"

send "1078 101 11 0 10000\n"
simple_expect "=1078 1 { 8 8 $any_time 00000000 }"


# Ensure that admin cannot set an invalid type

kom_logout
kom_login 5 "gazonk" 0
kom_enable 255

send "1079 102 8 11 00100000\n"
simple_expect "%1079 54 0"

send "1079 99 8 0 10000 0\n"
simple_expect "=1079 3 { 0 $any_time 11 201 0 0 \\* 8 $any_time 00000000 1 $any_time 9 200 0 0 \\* 7 $any_time 00100000 2 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"

send "1079 101 11 0 10000\n"
simple_expect "=1079 1 { 8 8 $any_time 00000000 }"


# Ensure that supervisor cannot set an invalid type

kom_logout
kom_login 14 "pw5" 0

send "1080 102 8 11 00100000\n"
simple_expect "%1080 54 0"

send "1081 99 8 0 10000 0\n"
simple_expect "=1081 3 { 0 $any_time 11 201 0 0 \\* 8 $any_time 00000000 1 $any_time 9 200 0 0 \\* 7 $any_time 00100000 2 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"

send "1082 101 11 0 10000\n"
simple_expect "=1082 1 { 8 8 $any_time 00000000 }"


# ----------------------------------------------------------------------
# 9. Check that we can set membership type as supervisor
#    Check that we can set membership type as admin
#    Check that we cannot set membership type as conference supervisor

kom_logout
kom_login 14 "pw5" 0

send "1090 102 8 9 00000000\n"
simple_expect "=1090"

send "1091 101 9 0 10000\n"
simple_expect "=1091 2 { 7 7 $any_time 00000000 8 7 $any_time 00000000 }"

send "1092 99 8 0 10000 0\n"
simple_expect "=1092 3 { 0 $any_time 11 201 0 0 \\* 8 $any_time 00000000 1 $any_time 9 200 0 0 \\* 7 $any_time 00000000 2 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"


kom_logout
kom_login 5 "gazonk" 0
kom_enable 255

send "1093 102 8 9 00100000\n"
simple_expect "=1093"

send "1094 101 9 0 10000\n"
simple_expect "=1094 2 { 7 7 $any_time 00000000 8 7 $any_time 00100000 }"

send "1095 99 8 0 10000 0\n"
simple_expect "=1095 3 { 0 $any_time 11 201 0 0 \\* 8 $any_time 00000000 1 $any_time 9 200 0 0 \\* 7 $any_time 00100000 2 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"


kom_logout
kom_login 7 "pw2" 0

send "1096 102 8 9 00000000\n"
simple_expect "%1096 12 8"

send "1097 101 9 0 10000\n"
simple_expect "=1097 2 { 7 7 $any_time 00000000 8 7 $any_time 00100000 }"

send "1098 99 8 0 10000 0\n"
simple_expect "=1098 3 { 0 $any_time 11 201 0 0 \\* 8 $any_time 00000000 1 $any_time 9 200 0 0 \\* 7 $any_time 00100000 2 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"



# ----------------------------------------------------------------------
# Test various things related to server configuration

shutdown_05
lyskomd_start "" \
"Add members by invitation: on
Allow reinvitations: off
Allow secret memberships: off
Garb interval: 9999999
Sync interval: 9999999
"

startup_05


# Test that we can force invitation bit

kom_logout
kom_login 8 "pw3" 0

send "2000 100 12 14 200 0 00000000\n"
lyskomd_expect "Person 14 added to conference 12 by 8."
simple_expect "=2000"

send "2001 99 14 0 10000 0\n"
simple_expect "=2001 2 { 0 $any_time 12 200 0 0 \\* 8 $any_time 10000000 1 $any_time 14 255 0 0 \\* 14 $any_time 00000000 }"

send "2002 101 12 0 10000\n"
simple_expect "=2002 1 { 14 8 $any_time 10000000 }"


# Test that we can change membership-type without changing invitation

kom_logout
kom_login 14 "pw5" 0

send "2003 102 14 12 10000001\n"
simple_expect "=2003"

send "2004 99 14 0 10000 0\n"
simple_expect "=2004 2 { 0 $any_time 12 200 0 0 \\* 8 $any_time 10000001 1 $any_time 14 255 0 0 \\* 14 $any_time 00000000 }"

send "2005 101 12 0 10000\n"
simple_expect "=2005 1 { 14 8 $any_time 10000001 }"


# Test that we can clear invitation bit

send "2006 102 14 12 00000000\n"
simple_expect "=2006"

send "2007 99 14 0 10000 0\n"
simple_expect "=2007 2 { 0 $any_time 12 200 0 0 \\* 8 $any_time 00000000 1 $any_time 14 255 0 0 \\* 14 $any_time 00000000 }"

send "2008 101 12 0 10000\n"
simple_expect "=2008 1 { 14 8 $any_time 00000000 }"


# Test that we can't set invitation bit

send "2009 102 14 12 10000000\n"
simple_expect "%2009 54 0"

send "2010 99 14 0 10000 0\n"
simple_expect "=2010 2 { 0 $any_time 12 200 0 0 \\* 8 $any_time 00000000 1 $any_time 14 255 0 0 \\* 14 $any_time 00000000 }"

send "2011 101 12 0 10000\n"
simple_expect "=2011 1 { 14 8 $any_time 00000000 }"


# Test that we can't change a membership to a secret membership

send "2009 102 14 12 00100000\n"
simple_expect "%2009 54 0"

send "2010 99 14 0 10000 0\n"
simple_expect "=2010 2 { 0 $any_time 12 200 0 0 \\* 8 $any_time 00000000 1 $any_time 14 255 0 0 \\* 14 $any_time 00000000 }"

send "2011 101 12 0 10000\n"
simple_expect "=2011 1 { 14 8 $any_time 00000000 }"


# Test that we can't create a secret membership

kom_logout
kom_login 8 "pw3" 0

send "2012 100 12 8 201 0 00100000\n"
simple_expect "%2012 54 0"

send "2013 99 8 0 10000 0\n"
simple_expect "=2013 1 { 0 $any_time 8 255 0 0 \\* 8 $any_time 00000000 }"

send "2014 101 12 0 10000\n"
simple_expect "=2014 1 { 14 8 $any_time 00000000 }"



# ======================================================================
# Test old-style calls

shutdown_05

lyskomd_start "" "Add members by invitation: on
Garb interval: 9999999
Sync interval: 9999999
"
startup_05

# ----------------------------------------------------------------------
# Test old-style add-member

kom_login 7 "pw2" 0

# Person 7 adds person 13 to conference 12
# Use new-style call to check that invitation bit is set

send "3000 14 12 13 200 0\n"
lyskomd_expect "Person 13 added to conference 12 by 7."
simple_expect "=3000"

send "3001 99 13 0 10000 0\n"
simple_expect "=3001 2 { 0 $any_time 12 200 0 0 \\* 7 $any_time 10000000 1 $any_time 13 255 0 0 \\* 8 $any_time 00000000 }"

send "3002 101 12 0 10000\n"
simple_expect "=3002 1 { 13 7 $any_time 10000000 }"

# Remove person 13 from conference 12

send "3003 15 12 13\n"
simple_expect "=3003"

send "3004 99 13 0 10000 0\n"
simple_expect "=3004 1 { 0 $any_time 13 255 0 0 \\* 8 $any_time 00000000 }"

send "3005 101 12 0 10000\n"
simple_expect "%3005 19 0"


# Person 8 adds person 13 to conference 12
# Use new-style call to check that invitation bit is unset

kom_logout
kom_login 8 "pw3" 0

send "3010 14 12 13 201 0\n"
lyskomd_expect "Person 13 added to conference 12 by 8."
simple_expect "=3010"

send "3011 99 13 0 10000 0\n"
simple_expect "=3011 2 { 0 $any_time 12 201 0 0 \\* 8 $any_time 00000000 1 $any_time 13 255 0 0 \\* 8 $any_time 00000000 }"

send "3012 101 12 0 10000\n"
simple_expect "=3012 1 { 13 8 $any_time 00000000 }"

# Remove person 13 from conference 12

send "3013 15 12 13\n"
simple_expect "=3013"

send "3014 99 13 0 10000 0\n"
simple_expect "=3014 1 { 0 $any_time 13 255 0 0 \\* 8 $any_time 00000000 }"

send "3015 101 12 0 10000\n"
simple_expect "%3015 19 0"


# Person 5 adds person 13 to conference 12
# Use new-style call to check that invitation bit is unset

kom_logout
kom_login 5 "gazonk" 0
kom_enable 255

send "3020 14 12 13 202 0\n"
lyskomd_expect "Person 13 added to conference 12 by 5."
simple_expect "=3020"

send "3021 99 13 0 10000 0\n"
simple_expect "=3021 2 { 0 $any_time 12 202 0 0 \\* 5 $any_time 00000000 1 $any_time 13 255 0 0 \\* 8 $any_time 00000000 }"

send "3022 101 12 0 10000\n"
simple_expect "=3022 1 { 13 5 $any_time 00000000 }"

# Remove person 13 from conference 12

send "3023 15 12 13\n"
simple_expect "=3023"

send "3024 99 13 0 10000 0\n"
simple_expect "=3024 1 { 0 $any_time 13 255 0 0 \\* 8 $any_time 00000000 }"

send "3025 101 12 0 10000\n"
simple_expect "%3025 19 0"


# Person 13 adds person 13 to conference 12
# Use new-style call to check that invitation bit is unset

kom_logout
kom_login 13 "pw4" 0

send "3030 14 12 13 203 0\n"
simple_expect "=3030"

send "3031 99 13 0 10000 0\n"
simple_expect "=3031 2 { 0 $any_time 12 203 0 0 \\* 13 $any_time 00000000 1 $any_time 13 255 0 0 \\* 8 $any_time 00000000 }"

send "3032 101 12 0 10000\n"
simple_expect "=3032 1 { 13 13 $any_time 00000000 }"


# ----------------------------------------------------------------------
# Test old-style get-members

# Person 13 sets passive bit of membership in conference 12
# Use old-style get-members on conference 12
# Use old-style get-membership on conference 12

kom_logout
kom_login 13 "pw4" 0

send "3100 102 13 12 01000000\n"
simple_expect "=3100"

send "3101 48 12 0 10000\n"
simple_expect "=3101 1 { 13 }"

send "3102 46 13 0 10000 0\n"
simple_expect "=3102 2 { $any_time 12 0 0 0 \\* $any_time 13 255 0 0 \\* }"


# Person 13 unsets passive bit of membership in conference 12
# Check that actual priority is now visible

send "3103 102 13 12 00000000\n"
simple_expect "=3103"

send "3104 48 12 0 10000\n"
simple_expect "=3104 1 { 13 }"

send "3105 46 13 0 10000 0\n"
simple_expect "=3105 2 { $any_time 12 203 0 0 \\* $any_time 13 255 0 0 \\* }"


# Use old-style add-member to change priority to 0
# Old-style calls should return same as previous test
# New-style get-membership should show passive bit set but priority unchanged
# New-style get-members should show passive bit set

send "3200 14 12 13 0 0\n"
simple_expect "=3200"

send "3201 48 12 0 10000\n"
simple_expect "=3201 1 { 13 }"

send "3202 46 13 0 10000 0\n"
simple_expect "=3202 2 { $any_time 12 0 0 0 \\* $any_time 13 255 0 0 \\* }"

send "3203 99 13 0 10000 0\n"
simple_expect "=3203 2 { 0 $any_time 12 203 0 0 \\* 13 $any_time 01000000 1 $any_time 13 255 0 0 \\* 8 $any_time 00000000 }"

send "3204 101 12 0 10000\n"
simple_expect "=3204 1 { 13 13 $any_time 01000000 }"


# ----------------------------------------------------------------------
# Prepare to test get-unread-confs and query-read-texts
# This setup assumes that set-membership-type, add-member and 
# create-text work.

kom_logout
kom_login 13 "pw4" 0

send "3300 102 13 12 00000000\n"
simple_expect "=3300"

send "3301 100 11 13 200 255 00000000\n"
simple_expect "=3301"

send "3302 86 [holl "Unread 1"] 1 { 0 12 } 0 { }\n"
simple_expect "=3302 1"

send "3303 86 [holl "Unread 2"] 1 { 0 11 } 0 { }\n"
simple_expect "=3303 2"


# ----------------------------------------------------------------------
# Test get-unread-confs

kom_logout
kom_login 13 "pw4" 0

send "3310 52 13\n"
simple_expect "=3310 2 { 12 11 }"

send "3311 102 13 12 01000000\n"
simple_expect "=3311"

send "3312 52 13\n"
simple_expect "=3312 1 { 11 }"

send "3313 102 13 11 01000000\n"
simple_expect "=3313"

send "3314 52 13\n"
simple_expect "=3314 0 \\*"

send "3315 102 13 12 00000000\n"
simple_expect "=3315"

send "3316 52 13\n"
simple_expect "=3316 1 { 12 }"

send "3317 102 13 11 00000000\n"
simple_expect "=3317"

send "3318 52 13\n"
simple_expect "=3318 2 { 12 11 }"


# ----------------------------------------------------------------------
# Test query-read-texts wrt membership positions

send "3400 99 13 0 10000 0\n"
simple_expect "=3400 3 { 0 $any_time 12 203 0 0 \\* 13 $any_time 00000000 1 $any_time 13 255 0 0 \\* 8 $any_time 00000000 2 $any_time 11 200 0 0 \\* 13 $any_time 00000000 }"

send "3401 98 13 12\n"
simple_expect "=3401 0 $any_time 12 203 0 0 \\* 13 $any_time 00000000"

send "3402 98 13 13\n"
simple_expect "=3402 1 $any_time 13 255 0 0 \\* 8 $any_time 00000000"

send "3403 98 13 11\n"
simple_expect "=3403 2 $any_time 11 200 0 0 \\* 13 $any_time 00000000"


# ----------------------------------------------------------------------
# We're finished
# ----------------------------------------------------------------------

shutdown_05
