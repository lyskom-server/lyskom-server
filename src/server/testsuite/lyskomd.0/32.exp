# Test suite for lyskomd.
# Copyright (C) 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test deletion of a person that is logged in on another session.


lyskomd_start

client_start 0
talk_to client 0
send "A\n"
send "0H\n"
simple_expect "LysKOM"
send "1000 62 5 6Hgazonk 0\n"
simple_expect ":2 9 5 1"
simple_expect "=1000"

# Create the user "First removed".  Log in as it.
client_start 1
talk_to client 1
send "A\n"
send "0H\n"
simple_expect "LysKOM"
send "1001 89 [holl "First removed"] [holl "none"] 00000000 0 { }\n"
simple_expect "=1001 6"
send "1002 62 6 [holl "none"] 0\n"
simple_expect ":2 9 6 2"
simple_expect "=1002"
talk_to client 0
simple_expect ":2 9 6 2"
talk_to client 1
# Make sure we get "permission-denied", not "login-first".
send "1003 7 5 00000000\n"
simple_expect "%1003 12 0"

# Remove "First removed".
talk_to client 0
send "1004 42 255\n"
simple_expect "=1004"
send "1005 11 6\n"
simple_expect ":2 13 6 2"
simple_expect "=1005"
lyskomd_expect "User 5 deleted conference 6\\."
lyskomd_expect "Conference 6 was named 'First removed'\\."
talk_to client 1
simple_expect ":2 13 6 2"
# Make sure we get "login-first".
send "1006 7 5 00000000\n"
simple_expect "%1006 6 0"

# Create the user "2nd removed".  Log in as it, and change-conference
# to the letterbox.
send "1007 89 [holl "2nd removed"] [holl "none"] 00000000 0 { }\n"
simple_expect "=1007 7"
send "1008 62 7 [holl "none"] 0\n"
simple_expect ":2 9 7 2"
simple_expect "=1008"
talk_to client 0
simple_expect ":2 9 7 2"
talk_to client 1
send "1009 2 7\n"
simple_expect "=1009"
# Make sure we get "permission-denied", not "login-first".
send "1010 7 5 00000000\n"
simple_expect "%1010 12 0"

# Remove "2nd removed".
talk_to client 0
send "1011 11 7\n"
simple_expect ":2 13 7 2"
simple_expect "=1011"
lyskomd_expect "User 5 deleted conference 7\\."
lyskomd_expect "Conference 7 was named '2nd removed'\\."
talk_to client 1
simple_expect ":2 13 7 2"
# Make sure we get "login-first".
send "1012 7 5 00000000\n"
simple_expect "%1012 6 0"

# Shut everything down.
talk_to client 0
send "1013 44 0\n"
simple_expect "=1013"
client_death 0
client_death 1
lyskomd_death
