# Test suite for lyskomd.
# Copyright (C) 1999-2000, 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Check async-sub-recipient.
#
# Three players are used:
#   client 0  person 5
#   client 1  person 6
#
#   conference 7    secret; created by 5; person 5 is member
#
# Person 5 is a member of 1, 5 and 7.
# Person 6 is a member of 1 and 6.
# 
lyskomd_start

# Establish the sessions and log in.
client_start 0
talk_to client 0
send "A3Hfoo\n"
simple_expect "LysKOM" "connected"
send "1000 80 5 { 14 15 16 17 18 }\n"
simple_expect "=1000"
send "1001 0 5 [holl gazonk]\n"
simple_expect "=1001"

client_start 1
talk_to client 1
send "A6Hgazonk\n"
simple_expect "LysKOM" "connected"
send "1002 80 5 { 14 15 16 17 18 }\n"
simple_expect "=1002"
send "1003 89 [holl "Unprivileged"] [holl "citronfromage"] 10000000 0 { }\n"
simple_expect "=1003 6"
send "1004 0 6 [holl citronfromage]\n"
simple_expect "=1004"

# Create the secret conference
talk_to client 0
send "1005 88 [holl "conf 7"] 1010 0 { }\n"
simple_expect "=1005 7"

# Join the conferences
talk_to client 0
send "1006 100 1 5 200 1 00000000\n"
simple_expect ":2 18 5 1"
simple_expect "=1006"
send "1007 100 7 5 200 2 00000000\n"
simple_expect ":2 18 5 7"
simple_expect "=1007"

talk_to client 1
send "1008 100 1 6 200 1 00000000\n"
simple_expect ":2 18 6 1"
simple_expect "=1008"

# Text 1: created by 5 (session 0)
#   rcpt 1<1>
#   cc   7<1>
#   bcc  6<1>
#   rcpt 2<1>

talk_to client 0
send "1009 86 [holl "text 1"] 4 { 0 1 1 7 15 6 0 2 } 0 { }\n"
extracting_expect ":18 15 1 ($any_time) 5 0 6 0 8 { 0 1 6 1 1 7 6 1 15 6 6 1 0 2 6 1 } 0 \\*" time_1 1
talk_to client 1
simple_expect ":18 15 1 $time_1 5 0 6 0 6 { 0 1 6 1 15 6 6 1 0 2 6 1 } 0 \\*"
talk_to client 0
simple_expect "=1009 1"

# Remove "cc 7<1>".
send "1010 31 1 7\n"
simple_expect ":3 17 1 7 1"

talk_to client 0
simple_expect "=1010"

talk_to client 1
send "1011 35\n"
simple_expect "=1011 $any_time"

# Remove "bcc 6<1>".
talk_to client 0
send "1012 31 1 6\n"
simple_expect ":3 17 1 6 15"
talk_to client 1
simple_expect ":3 17 1 6 15"
talk_to client 0
simple_expect "=1012"

# Remove "rcpt 2<1>".
send "1013 31 1 2\n"
simple_expect ":3 17 1 2 0"
talk_to client 1
simple_expect ":3 17 1 2 0"
talk_to client 0
simple_expect "=1013"

talk_to client 1
send "1014 35\n"
simple_expect "=1014 $any_time"
talk_to client 0
send "1015 90 1\n"
simple_expect "=1015 $time_1 5 0 6 0 2 { 0 1 6 1 } 0 \\*"
send "1016 56\n"
simple_expect "=1016 1"
send "1017 64 1\n"
simple_expect "=1017 5 0 1 0H $hollerith $hollerith $hollerith $any_num $any_time"
send "1018 98 5 1\n"
simple_expect "=1018 1 $any_time 1 200 0 0 \\* 5 $any_time 00000000"

send "1019 31 1 1\n"
simple_expect ":3 17 1 1 0"
talk_to client 1
simple_expect ":3 17 1 1 0"
talk_to client 0
simple_expect "=1019"

# Shut down the server.

talk_to client 1
send "1020 55 0\n"
simple_expect "=1020"
client_death 1

talk_to client 0

send "1021 42 255\n"
simple_expect "=1021"
send "1022 44 0\n"
simple_expect "=1022"
client_death 0

lyskomd_death
