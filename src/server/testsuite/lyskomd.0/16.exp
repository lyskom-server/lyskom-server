# Test suite for lyskomd.
# Copyright (C) 1999-2000, 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Check if secret conference numbers can leak from the various
# calls that get session information.

# Several players are used:
#   client 0  person 6  member of 6
#   client 1  person 7  member of 7, 8, creator of 8
#
# Conferences:
#   8 (secret)  members: 7  creator: 7

proc assert_0 {conf msg} {
    # Fail if $conf is non-zero.

    if {$conf == 0} {
	pass "$msg"
    } else {
	fail "$msg"
    }
}

lyskomd_start

# Establish the sessions, create persons, and log in.
proc startup_16 {session person} {
    client_start $session
    talk_to client $session
    send "A3Hfoo\n"
    simple_expect "LysKOM" "connected"
    send "100 80 1 { 6 }\n"
    simple_expect "=100"
    send "101 89 [holl "Person $person"] [holl "foo"] 00000000 0 { }\n"
    simple_expect "=101 $person"
    send "102 62 $person [holl "foo"] 0\n"
    simple_expect "=102"
}

startup_16 0 6
startup_16 1 7

# Create the conference
talk_to client 1
send "1000 88 [holl "conf 8"] 1010 0 { }\n"
simple_expect "=1000 8"

# Join the conference
talk_to client 1
send "1001 100 8 7 100 1 00000000\n"
simple_expect "=1001"

# Person 7 goes to the secret conference.
send "1002 2 8\n"
simple_expect ":5 6 7 8 2 0H [idholl "foo"]"
simple_expect "=1002"
talk_to client 0
# Should this even be sent?  Seen from the view-point of person 6,
# person 7 goes from conference 0 to conference 0.
extracting_expect ":5 6 7 (\[08\]) 2 0H [idholl "foo"]" conf 1
assert_0 $conf "async-i-am-on leaks secret info"


# Person 6 tries to get information.

# Assert that conference 8 is invisible to person 6.
send "1003 91 8\n"
simple_expect "%1003 9 8"

send "1004 39\n"
extracting_expect "=1004 2 { 7 (\[08\]) 0H 6 0 0H }" conf 1
assert_0 $conf "who-is-on-old leaks secret info"

send "1005 51\n"
extracting_expect "=1005 2 { 7 (\[08\]) 2 0H [idholl "foo"] 6 0 1 0H [idholl "foo"] }" conf 1
assert_0 $conf "who-is-on leaks secret info"

send "1007 54 2\n"
extracting_expect "=1007 7 (\[08\]) 2 0H [idholl "foo"] $any_num $any_time" conf 1
assert_0 $conf "get-session-info leaks secret info"

send "1006 63\n"
extracting_expect "=1006 2 { 7 (\[08\]) 2 0H 3Hfoo [holl "$lyskomd_host"] [holl "unknown"] 6 0 1 0H 3Hfoo [holl "$lyskomd_host"] [holl "unknown"] }" conf 1
assert_0 $conf "who-is-on-ident leaks secret info"

send "1007 64 2\n"
extracting_expect "=1007 7 (\[08\]) 2 0H 3Hfoo [holl "$lyskomd_host"] [holl "unknown"] $any_num $any_time" conf 1
assert_0 $conf "get-session-info-ident leaks secret info"

send "1 83 1 1 0\n"
extracting_expect "=1 2 { 2 7 (\[08\]) $any_num 00000000 0H 1 6 0 $any_num 00000000 0H }" conf 1
assert_0 $conf "who-is-on-dynamic leaks secret info"

# Shut down the server.

talk_to client 1
send "1008 55 0\n"
simple_expect "=1008"
client_death 1

talk_to client 0
send "1009 62 5 [holl "gazonk"] 1\n"
simple_expect "=1009"
send "1010 42 255\n"
simple_expect "=1010"
send "1011 44 0\n"
simple_expect "=1011"
client_death 0

lyskomd_death
