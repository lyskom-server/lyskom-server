# expect 5.44.1.1 introduced a "gate keeper glob pattern", to avoid
# using "slow" regexp matching.  Unfortunately, the glob pattern
# constructed for many of our fast regular expressions are very, very
# slow.  So slow that the test suite cannot continue.
#
# This test reproduces the problem to a small extent.  In expect
# 5.43.0 and earlier this runs in less than 1 ms, but in 5.44.1.14 it
# takes 1.3 seconds (on a particular computer).
#
# The bug is reported here:
# http://sourceforge.net/tracker/?func=detail&atid=113179&aid=3010684&group_id=13179
#
# This issue is fixed in expect 5.45, released 2010-11-09.
#
# Debian has backported the fix to version 5.44.1.15-3 (see
# <http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=591695>).
#
# Ubuntu has the backported fix in Ubuntu 11.04 or newer (see
# <https://bugs.launchpad.net/ubuntu/+source/expect/+bug/612523>).

spawn echo "MRK:client1: :18 14 6 6 3 22 1 5 110 2 151 1 5 0 8 0 2 { 0 1 6 5 } 0 *\nMRK:client1: :18 14 7 6 3 22 1\n"
set t0 [clock clicks -milliseconds]
expect {
    -re "^MRK:client1: :18 14 6 \[0-9\]* \[0-9\]* \[0-9\]* \[0-9\]* \[0-9\]* \[0-9\]* \[0-9\]* \[0-9\]* \[0-9\]* 5 0 8 0 2 { 0 1 6 5 } 0 \\\\*\r?\n" {
    }
}
set t1 [clock clicks -milliseconds]
if {$t1 - $t0 > 20} {
    fail "regexp matching too slow ([expr $t1 - $t0] ms)"
} else {
    pass "regexp matching seems to be fast"
}
