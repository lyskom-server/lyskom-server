set height 0

break trace_smalloc
commands
echo --- malloc ---\n
bt
cont
end

break trace_free
commands
echo --- free ---\n
bt
cont
end

break trace_srealloc
commands
echo --- realloc ---\n
bt
cont
end
