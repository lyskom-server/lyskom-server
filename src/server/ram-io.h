/* Store database file state.
 * Copyright (C) 2006  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

/* Store state needed to access a database status file.  There can be
   several files opened at the same time, with different formats.  This
   struct keeps track of the FILE* and format version. */

struct dbfile {
    FILE *fp;
    int format;
};

/* Close the file referenced by obj (if fp != NULL), and free the
   struct dbfile. */
int dbfile_delete(struct dbfile *obj);

/* Set the default output format for all future calls to
   dbfile_open_write() (until the next call to
   set_output_format()). */
void set_output_format(int new_format);

/* Open the file for writing, and write a header that contains the
   specified magic marker.  The magic string must be 5 characters
   long. */
extern struct dbfile *
dbfile_open_write(const char *filename,
		  const char *magic);

/* Change the magic cookie in the file.  This is typically done to
   mark the file as clean once it is completely written.  The magic
   string must be 5 characters long. */
Success
dbfile_change_magic(struct dbfile *fp,
		    const char *magic);

/* Open the file for reading.  The file format version will be read
   from the file.

   wanted_magic should be a NUL-terminated string of five characters.
   If the file contains something else an error will be logged and
   NULL returned.  wanted_magic can also be NULL, to open the file
   regardless of the magic string.  However, all files are expected to
   have a valid file header.
*/
extern struct dbfile *
dbfile_open_read(const char *filename,
		 const char *wanted_magic);

/* Dump information about the number of existing dbfile structs and
   open dbfile file descriptors to the specified file. */
void
dump_dbfile_stats(FILE *fp);

/* Call ftell() on the file. */
long
dbfile_ftell(struct dbfile *fp);

/* Call getc() on the file. */
int
dbfile_getc(struct dbfile *fp);

/* Call fputs() on the file. */
int
dbfile_fputs(const char *s,
	     struct dbfile *fp);

/* Call putc() on the file. */
int
dbfile_putc(int c, struct dbfile *fp);

/* Call ungetc() on the file. */
int
dbfile_ungetc(struct dbfile *fp, int c);

/* Call feof() on the file. */
int
dbfile_feof(struct dbfile *fp);

/* Call ferror() on the file. */
int
dbfile_ferror(struct dbfile *fp);
