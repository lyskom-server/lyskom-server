/*
 * $Id: ram-output.h,v 0.17 2003/08/23 16:38:14 ceder Exp $
 * Copyright (C) 1991, 1993-1999, 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: ram-output.h,v 0.17 2003/08/23 16:38:14 ceder Exp $
 *
 * ram-output.c  -  write objects to disk.
 *
 * This is a hack. It shouldn't be used except for debugging and as a
 * temporary substitute for what Willf|r is (or should:-) be doing.
 *
 * Written by ceder 1990-07-13. Rewritten 1990-08-31.
 */

extern void
foutput_header(struct dbfile *fp, const char *state, int include_timestamp);

extern void
foutput_info(struct dbfile *fp,
             Info *info);

extern void
foutput_person(struct dbfile *fp,
	       const Person *person);

extern void
foutput_conference(struct dbfile *fp,
		   Conference *conf_c);

extern void
foutput_text_stat(struct dbfile *fp,
		  Text_stat *t_stat);

/* Needed by debug code in membership.c.  */
extern void
foutput_membership(struct dbfile *fp,
                   Membership *mship);

extern void
foutput_atsign(struct dbfile *fp);

extern void
foutput_newline(struct dbfile *fp);
