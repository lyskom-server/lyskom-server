/*
 * $Id: ram-parse.h,v 0.14 2003/08/23 16:38:14 ceder Exp $
 * Copyright (C) 1991, 1993-1995, 1997-1999, 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: ram-parse.h,v 0.14 2003/08/23 16:38:14 ceder Exp $
 *
 * ram-parse.h -- parse objects from disk file.
 */

extern unsigned long
fparse_long(struct dbfile *fp);

extern void
fskipwhite(struct dbfile *fp);

extern time_t
fparse_time(struct dbfile *fp);

extern Success
fparse_info(struct dbfile *fp,
            Info *info);

extern Success
fparse_conference(struct dbfile *fp,
		  Conference *result);


Success
fparse_person(struct dbfile *fp,
	      Person *person);

	
extern Success
fparse_membership_list(struct dbfile *fp,
		       Membership_list *result);


extern Success
fparse_conf_list(struct dbfile *fp,
		 Conf_list_old *result);


extern Success
fparse_mark_list(struct dbfile *fp,
		 Mark_list *result);


extern Success
fparse_text_stat(struct dbfile *fp,
		 Text_stat *result);


extern Success
fparse_info(struct dbfile *fp,
	    Info *result);


extern Success
fparse_string(struct dbfile *fp,
	      String *result);

extern Success
fparse_member_list(struct dbfile *fp,
		   Member_list *result);


extern Success
fparse_member(struct dbfile *fp,
	      Member *result);

extern Success
fparse_mark(struct dbfile *fp,
	    Mark *result);


extern Success
fparse_priv_bits(struct dbfile *fp,
		 Priv_bits *result);


extern Success
fparse_personal_flags(struct dbfile *fp,
		      Personal_flags *result);

extern Success
fparse_conf_type(struct dbfile *fp,
		 Conf_type *result);


extern Success
fparse_who_info(struct dbfile *fp,
		Who_info *result);

    
extern Success
fparse_who_info_list(struct dbfile *fp,
		     Who_info_list *result);

extern Success
fparse_aux_item_flags(struct dbfile *fp,
                      Aux_item_flags *f);

extern Success
fparse_aux_item_link(struct dbfile *fp,
                     Aux_item_link *link);
extern Success
fparse_aux_item(struct dbfile *fp, 
                Aux_item *result);

extern Success
fparse_aux_item_list(struct dbfile *fp,
		     Aux_item_list *result);

extern Success
fparse_misc_info(struct dbfile *fp, 
		 Misc_info *result);

extern Success
fparse_set_pos(struct dbfile *fp,
	       long offset);
