/*
 * $Id: end-of-atomic.h,v 0.7 2003/08/23 16:38:17 ceder Exp $
 * Copyright (C) 1991, 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: end-of-atomic.h,v 0.7 2003/08/23 16:38:17 ceder Exp $
 *
 * end-of-atomic.c
 */

/*
 * Clean up after an atomic request.  This will throw things from the
 * cache, free memory allocated with tmp_alloc(), and save part of the
 * database.  The function should be called after every atomic
 * request, and it should also be called periodically; the caller
 * should wait no more than the return value before the function is
 * called again.
 */
struct timeval
end_of_atomic(void);
