/*
 * $Id: prot-a-send-async.c,v 0.41 2005/12/18 22:18:14 ceder Exp $
 * Copyright (C) 1991, 1993-1999, 2001-2005  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * Asynchronous messages in protocol A.
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <setjmp.h>
#include "timewrap.h"
#include <sys/types.h>
#include <sys/socket.h>
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif

#include "oop.h"

#include "misc-types.h"
#include "s-string.h"
#include "kom-types.h"
#include "com.h"
#include "async.h"
#include "connections.h"
#include "prot-a-send-async.h"
#include "async.h"
#include "isc-interface.h"
#include "prot-a-output.h"


static void
async_header(Connection *fp,
	     int   no_of_tokens,
	     enum async fnc)
{
    isc_putc(':', fp->isc_session);
    isc_putul(no_of_tokens, fp->isc_session);
    prot_a_output_ul(fp, fnc);
}



    
static void
async_trailer(Connection *fp)
{
    isc_putc('\n', fp->isc_session);
    /* FIXME (bug 107): Maybe we shouldn't flush each and every
       asynchronous message at once.  We would probably get a better
       performance if we spread them out over a few seconds.  */
    isc_flush(fp->isc_session);
}


void
prot_a_async_new_text_old(Connection *cptr,
                          Text_no	      text_no,
                          Text_stat      *text_s)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_new_text_old);
    async_header(cptr, 16, ay_new_text_old);
    prot_a_output_ul(cptr, text_no);
    prot_a_output_text_stat_old(cptr, text_s);
    async_trailer(cptr);
}

void
prot_a_async_new_text(Connection *cptr,
                      Text_no	      text_no,
                      Text_stat      *text_s)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_new_text);
    async_header(cptr, 18, ay_new_text);
    prot_a_output_ul(cptr, text_no);
    prot_a_output_text_stat(cptr, text_s);
    async_trailer(cptr);
}


void
prot_a_async_i_am_on(Connection *cptr,
		     Who_info info)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_i_am_on);
    async_header(cptr, 5, ay_i_am_on);
    prot_a_output_who_info(cptr, &info);
    async_trailer(cptr);
}

void
prot_a_async_logout(Connection *cptr,
		    Pers_no pers_no,
		    Session_no session_no)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_logout);
    async_header(cptr, 2, ay_logout);
    prot_a_output_ul(cptr, pers_no);
    prot_a_output_ul(cptr, session_no);
    async_trailer(cptr);
}

void
prot_a_async_new_name(Connection *cptr,
		      Conf_no conf_no,
		      String old_name,
		      String new_name)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_new_name);
    async_header(cptr, 3, ay_new_name);
    prot_a_output_ul(cptr, conf_no);
    prot_a_output_string(cptr, old_name);
    prot_a_output_string(cptr, new_name);
    async_trailer(cptr);
}

void
prot_a_async_sync_db(Connection *cptr)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_sync_db);
    async_header(cptr, 0, ay_sync_db);
    async_trailer(cptr);
}

void
prot_a_async_forced_leave_conf(Connection *cptr,
			       Conf_no         conf_no)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_leave_conf);
    async_header(cptr, 1, ay_leave_conf);
    prot_a_output_ul(cptr, conf_no);
    async_trailer(cptr);
}

void
prot_a_async_login(Connection *cptr,
		   Pers_no	   pers_no,
		   int		   session_no)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_login);
    async_header(cptr, 2, ay_login);
    prot_a_output_ul(cptr, pers_no);
    prot_a_output_ul(cptr, session_no);
    async_trailer(cptr);
}

void
prot_a_async_rejected_connection(Connection *cptr)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_rejected_connection);
    async_header(cptr, 0, ay_rejected_connection);
    async_trailer(cptr);
}
    
void
prot_a_async_send_message(Connection *cptr,
			  Conf_no     recipient,
			  Pers_no     sender,
			  String      message)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_send_message);
    async_header(cptr, 3, ay_send_message);
    prot_a_output_ul(cptr, recipient);
    prot_a_output_ul(cptr, sender);
    prot_a_output_string(cptr, message);
    async_trailer(cptr);
}

void
prot_a_async_deleted_text(Connection *cptr,
                         Text_no	      text_no,
                         Text_stat      *text_s)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_deleted_text);
    async_header(cptr, 18, ay_deleted_text);
    prot_a_output_ul(cptr, text_no);
    prot_a_output_text_stat(cptr, text_s);
    async_trailer(cptr);
}

void
prot_a_async_new_recipient(Connection     *cptr,
                           Text_no        text_no,
                           Conf_no        conf_no,
                           enum info_type type)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_new_recipient);
    async_header(cptr, 3, ay_new_recipient);
    prot_a_output_ul(cptr, text_no);
    prot_a_output_ul(cptr, conf_no);
    prot_a_output_ul(cptr, type);
    async_trailer(cptr);
}

void
prot_a_async_sub_recipient(Connection     *cptr,
                           Text_no        text_no,
                           Conf_no        conf_no,
                           enum info_type type)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_sub_recipient);
    async_header(cptr, 3, ay_sub_recipient);
    prot_a_output_ul(cptr, text_no);
    prot_a_output_ul(cptr, conf_no);
    prot_a_output_ul(cptr, type);
    async_trailer(cptr);
}

void
prot_a_async_new_membership(Connection   *cptr,
                            Pers_no pers_no,
                            Conf_no conf_no)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_new_membership);
    async_header(cptr, 2, ay_new_membership);
    prot_a_output_ul(cptr, pers_no);
    prot_a_output_ul(cptr, conf_no);
    async_trailer(cptr);
}

void
prot_a_async_new_user_area(Connection   *cptr,
			   Pers_no       pers_no,
			   Text_no       old_user_area,
			   Text_no       new_user_area)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_new_user_area);
    async_header(cptr, 3, ay_new_user_area);
    prot_a_output_ul(cptr, pers_no);
    prot_a_output_ul(cptr, old_user_area);
    prot_a_output_ul(cptr, new_user_area);
    async_trailer(cptr);
}

void
prot_a_async_new_presentation(Connection   *cptr,
			      Conf_no       conf_no,
			      Text_no       old_presentation,
			      Text_no       new_presentation)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_new_presentation);
    async_header(cptr, 3, ay_new_presentation);
    prot_a_output_ul(cptr, conf_no);
    prot_a_output_ul(cptr, old_presentation);
    prot_a_output_ul(cptr, new_presentation);
    async_trailer(cptr);
}

void
prot_a_async_new_motd(Connection   *cptr,
		      Conf_no       conf_no,
		      Text_no       old_motd,
		      Text_no       new_motd)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_new_motd);
    async_header(cptr, 3, ay_new_motd);
    prot_a_output_ul(cptr, conf_no);
    prot_a_output_ul(cptr, old_motd);
    prot_a_output_ul(cptr, new_motd);
    async_trailer(cptr);
}

void
prot_a_async_text_aux_changed(Connection    *cptr,
			      Text_no        text_no,
			      Aux_item_list *aux_list,
			      unsigned long  highest_old_aux)
{
    unsigned short added = 0;
    unsigned short deleted = 0;
    unsigned short ix;
    
    ASYNC_CHECK_ACCEPT(cptr, ay_text_aux_changed);

    for (ix = 0; ix < aux_list->length; ix++)
    {
	if (aux_list->items[ix].flags.deleted)
	    deleted++;
	if (aux_list->items[ix].aux_no > highest_old_aux)
	    added++;
    }

    if (deleted + added == 0)
	return;
    
    async_header(cptr, 5, ay_text_aux_changed);

    prot_a_output_ul(cptr, text_no);

    prot_a_output_ul(cptr, deleted);
    if (deleted == 0)
	isc_puts(" *", cptr->isc_session);
    else
    {
	isc_puts(" {", cptr->isc_session);
	for (ix = 0; ix < aux_list->length; ix++)
	    if (aux_list->items[ix].flags.deleted)
		prot_a_output_aux_item(cptr, &aux_list->items[ix]);
	isc_puts(" }", cptr->isc_session);
    }

    prot_a_output_ul(cptr, added);
    if (added == 0)
	isc_puts(" *", cptr->isc_session);
    else
    {
	isc_puts(" {", cptr->isc_session);
	for (ix = 0; ix < aux_list->length; ix++)
	    if (aux_list->items[ix].aux_no > highest_old_aux)
		prot_a_output_aux_item(cptr, &aux_list->items[ix]);
	isc_puts(" }", cptr->isc_session);
    }
    async_trailer(cptr);
}

void
prot_a_async_text_read(Connection   *cptr,
		       Conf_no       conf_no,
		       Local_text_no local_no)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_text_read);
    async_header(cptr, 2, ay_text_read);
    /* The protocol allows for a list of {conf_no, local_no} pairs,
       but the current implementation is a lot more simpleminded... */
    isc_puts(" 1 {", cptr->isc_session);
    prot_a_output_ul(cptr, conf_no);
    prot_a_output_ul(cptr, local_no);
    isc_puts(" }", cptr->isc_session);
    async_trailer(cptr);
}

void
prot_a_async_invalidate_text_read(Connection   *cptr,
				  Conf_no       conf_no)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_invalidate_text_read);
    async_header(cptr, 1, ay_invalidate_text_read);
    prot_a_output_ul(cptr, conf_no);
    async_trailer(cptr);
}

#ifdef DEBUG_CALLS

void
prot_a_async_garb_ended(Connection *cptr,
			int deleted_texts)
{
    ASYNC_CHECK_ACCEPT(cptr, ay_garb_ended);
    async_header(cptr, 1, ay_garb_ended);
    prot_a_output_ul(cptr, deleted_texts);
    async_trailer(cptr);
}

#endif
