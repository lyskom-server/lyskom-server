/*
 * $Id: log.h,v 0.17 2003/08/23 16:38:16 ceder Exp $
 * Copyright (C) 1991, 1993-1996, 1999, 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: log.h,v 0.17 2003/08/23 16:38:16 ceder Exp $
 *
 * log.h
 *
 * File created by ceder 1990-05-25.
 */

#ifndef LYSKOM_H_INCLUDED
#define LYSKOM_H_INCLUDED

/*
 * Add a string to the log file.
 */

#if defined(HAVE_VFPRINTF) && defined(HAVE_STDARG_H)
extern void
kom_log (const char * format, ...)
#  if HAVE_ATTRIBUTE_FORMAT_PRINTF
    __attribute__ ((format (printf, 1, 2)))
#  endif
    ;

#else  /* !HAVE_VFPRINTF || !HAVE_STDARG_H*/
extern void
kom_log ();

#endif /* !HAVE_VFPRINTF || !HAVE_STDARG_H*/

#endif
