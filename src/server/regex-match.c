/*
 * $Id: regex-match.c,v 1.38 2003/08/23 16:38:14 ceder Exp $
 * Copyright (C) 1992-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

/*
 * Regexp matching
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#include <setjmp.h>
#include <sys/types.h>
#include "timewrap.h"
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif
#ifdef HAVE_STRING_H
#  include <string.h>
#endif

#include "s-string.h"
#include "misc-types.h"
#include "kom-types.h"
#include "services.h"
#include "regex.h"
#include "server/smalloc.h"
#include "cache.h"
#include "kom-errno.h"
#include "com.h"
#include "async.h"
#include "connections.h"
#include "manipulate.h"
#include "log.h"
#include "kom-config.h"
#include "conf-file.h"
#include "param.h"

static Success
lookup_regexp (Connection        *conn,
               const String       regexp,
	       Conf_z_info_list  *result,
	       Bool	          want_persons,
	       Bool	          want_confs)
{
    struct re_pattern_buffer pat_buf;
    Conf_no conf_no;
    String name = EMPTY_STRING;
    const char *errmsg;
    Conf_type type;

    /* FIXME (bug 163): It is unnecessary to allocate this much if
       only one conference matches. */
    result->confs = tmp_alloc (cached_no_of_existing_conferences()
			       * sizeof(Conf_z_info));
    result->no_of_confs = 0;

    re_syntax_options = RE_SYNTAX_GREP;

    pat_buf.translate = NULL;

    /* We have to use malloc() instead of smalloc() when allocating
       the fastmap, since regfree will use free() to free the memory.
       If malloc fails here we simply continue without a fastmap.  The
       match will be a little slower, but that is not fatal.  */
    pat_buf.fastmap = malloc(256);
    pat_buf.allocated = 0;
    pat_buf.buffer = 0;

    if ((errmsg =
	 re_compile_pattern((char*)regexp.string, s_strlen(regexp), &pat_buf))
	!= NULL)
    {
	regfree(&pat_buf);
        err_stat = 0;
	kom_errno = KOM_REGEX_ERROR;
	return FAILURE;
    }

    for (conf_no = 0; (conf_no = traverse_conference(conf_no)) != 0;)
    {
	type = cached_get_conf_type(conf_no);
	if ((type.letter_box ? want_persons : want_confs)
	    && has_access(conf_no, conn, read_protected))
	{
	    name = cached_get_name(conf_no);
	    switch ( re_search (&pat_buf,
				(char*)name.string, s_strlen(name), 0,
				s_strlen(name), NULL) )
	    {
	    case -1:
		break;
	    case -2:
		kom_log("Internal error in regex.\n");
		break;
	    default:
		/* It matched. (Ignore where it matched). */
		result->confs[result->no_of_confs].conf_no = conf_no;
		result->confs[result->no_of_confs].name = name;
		result->confs[result->no_of_confs++].type = type;
		break;
	    }
	}
    }

    regfree(&pat_buf);

    return OK;
}

static void
downgrade(Conf_z_info_list *in,
	  Conf_no_list *out)
{
    int ix;

    out->conf_nos = tmp_alloc(sizeof(Conf_no) * in->no_of_confs);
    out->no_of_confs = in->no_of_confs;
    for (ix = 0; ix < in->no_of_confs; ix++)
    {
	out->conf_nos[ix] = in->confs[ix].conf_no;
    }
}

Success
re_lookup_person (const String  regexp,
		  Conf_no_list  *result)
{
    Conf_z_info_list tmp;
    Success retval;

    CHK_CONNECTION(FAILURE);
    retval = lookup_regexp(active_connection, regexp, &tmp, TRUE, FALSE);
    if (retval == OK)
	downgrade(&tmp, result);
    return retval;
}


Success
re_lookup_conf (const String  regexp,
		Conf_no_list  *result)
{
    Conf_z_info_list tmp;
    Success retval;

    CHK_CONNECTION(FAILURE);
    retval = lookup_regexp(active_connection, regexp, &tmp, FALSE, TRUE);
    if (retval == OK)
	downgrade(&tmp, result);
    return retval;
}

Success
re_z_lookup (const String  regexp,
	     int want_persons,
	     int want_confs,
	     Conf_z_info_list *result)
{
    CHK_CONNECTION(FAILURE);
    CHK_BOOL(want_persons, FAILURE);
    CHK_BOOL(want_confs, FAILURE);
    return lookup_regexp(active_connection, regexp, result,
                         want_persons, want_confs);
}
