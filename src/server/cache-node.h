/*
 * $Id: cache-node.h,v 0.18 2003/08/23 16:38:18 ceder Exp $
 * Copyright (C) 1991, 1993-1995, 1997, 1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: cache-node.h,v 0.18 2003/08/23 16:38:18 ceder Exp $
 *
 * cache-node.h
 */

typedef struct cache_node {
    struct {
	unsigned int	exists : 1;
	unsigned int	dirty : 1;    /* Is *ptr modified? */
    } s;
    void   *snap_shot;		/* Dirty data to be written to file B. */
				/* (Dirty relative to file A). */
    void   *ptr;		/* In-core data. */
    long   pos;			/* Position to element in file A. */
    long   size;		/* Size on disk. */
#ifdef FASTSAVE
    long   saved_pos;           /* Position saved in case of recover */
    long   saved_size;          /* Size saved in case of recover */
#else
    long   pos_b;		/* Position to element in file B. */
    long   size_b;		/* Size in file B. */
#endif
    struct cache_node *prev;	/* Points towards most recently used. */
    struct cache_node *next;	/* Points towards least recently used. */
    int	lock_cnt;
} Cache_node;

extern const Cache_node EMPTY_CACHE_NODE;


typedef struct cache_node_block {
    int next_free;
    Cache_node *nodes;		/* Pointer to an array with
				   mcb_size elements. */
    struct cache_node_block *link; /* Points to previous block. */
} Cache_node_block;

extern const Cache_node_block EMPTY_CACHE_NODE_BLOCK;
	  

typedef struct cache_node_mcb {
    int mcb_size;		/* Number of cache_nodes in each block. */
    Cache_node_block *last_block; /* The block where new cache_nodes
				   * can be allocated. */
    unsigned long hits;
    unsigned long misses;
    Cache_node	*mru;
    Cache_node	*lru;
    unsigned long lookup_table_size;
    Cache_node	**lookup_table;	/* Easy to implement, but memory expensive. */
} Cache_node_mcb;


extern const Cache_node_mcb EMPTY_CACHE_NODE_MCB;

extern  void
unlink_lru(Cache_node *node,
	   Cache_node **lru,
	   Cache_node **mru);


extern Cache_node_mcb *
create_cache_node_mcb(int mcb_size, 
		      int table_size);


extern void
destruct_cache_node(Cache_node_mcb  *control,
		    unsigned long           key);

extern Cache_node *
get_cache_node (Cache_node_mcb *control,
		unsigned long          key);


extern  void
create_cache_node (Cache_node_mcb *control,
		   unsigned long          key);


extern  void
zero_init_cache_node (Cache_node_mcb *control,
		      unsigned long          key);

extern void
set_mru(Cache_node_mcb *mcb,
	unsigned long         key);


extern void
free_cache_node_mcb(Cache_node_mcb *control);
