# $Id: Makefile.am,v 1.68 2003/10/03 08:42:34 ceder Exp $
# Copyright (C) 1998-1999, 2001-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 
#

include $(top_srcdir)/scripts/common.make

SUBDIRS = testsuite

AM_YFLAGS = -d

EXTRA_DIST = ChangeLog.1 Magics \
	call-switch.awk com-h.awk fnc-def-init.awk \
	prot-a-parse-arg-c.awk prot-a-parse-arg-h.awk \
	prot-a-is-legal-fnc.awk \
	fncdef.txt \
	handle-malloc-dump.el trace-mem.gdb

MOSTLYCLEANFILES = .gdbinit call-switch.incl com.h fnc-def-init.incl \
	fncdef-no-str-limit.txt prot-a-parse-arg.h version.incl \
	prot-a-is-legal-fnc.incl *.da *.bb *.gcov *.bbg \
	version-info.c prot-a-parse-arg.c aux-no.h paths.h

AM_CPPFLAGS = \
	-I../libraries/libeintr \
	-I$(srcdir)/../include \
	-I$(srcdir)/../libraries/libansi \
	-I$(srcdir)/../libraries/libmisc \
	-I$(srcdir)/../libraries/liboop \
	-I$(srcdir)/../libraries/adns/src \
	-I$(srcdir)/../libraries/libisc-new/src \
	-I$(srcdir)/../libraries/regex \
	-I$(srcdir)/../libraries/libcommon

top_srcdir = @top_srcdir@

sbin_PROGRAMS = lyskomd dbck updateLysKOM komrunning splitkomdb
CHECKKOMSPACE =

if HAVE_FSUSAGE
sbin_PROGRAMS += checkkomspace
CHECKKOMSPACE += checkkomspace$(EXEEXT)
endif

check_LIBRARIES = libcheck.a
libcheck_a_SOURCES = local-to-global.c ram-parse.c ram-output.c \
	log.c ram-smalloc.c memory.c getopt.c getopt1.c \
	misc-types.c ram-io.c server-time.c

lyskomd_SOURCES = $(DISKOBJS) $(GENOBJS) \
        aux-item-def-parse.y \
        aux-item-def-parse.h \
	aux-item-def-scan.l \
	admin.h async.h aux-items.h cache-node.h cache.h conf-file.h \
	connections.h end-of-atomic.h exp.h internal-connections.h \
	text.h isc-interface.h isc-malloc.h isc-parse.h \
	kom-memory.h local-to-global.h log.h lyskomd.h manipulate.h \
	minmax.h param.h prot-a-output.h prot-a-parse.h \
	prot-a-send-async.h prot-a.h \
	ram-io.h ram-io.c ram-output.h ram-parse.h \
	rfc931.h send-async.h server-config.h string-malloc.h \
	text-garb.h version-info.h sigflags.h trace-alloc.h \
	lockdb.h lockdb.c server-time.h oop-malloc.h oop-malloc.c \
	timewrap.h stats.h stats.c \
	misc-types.c
nodist_lyskomd_SOURCES = $(NODIST_DISKOBJS) $(NODIST_GENOBJS)

READ_CONFIG = string-malloc.c ram-smalloc.c conf-file.c server-config.c \
	 standalone.c

komrunning_SOURCES = komrunning.c pidfile.c pidfile.h $(READ_CONFIG) \
	misc-types.c log.c

updateLysKOM_SOURCES = updateLysKOM.c pidfile.c pidfile.h $(READ_CONFIG) \
	misc-types.c log.c

checkkomspace_SOURCES = checkkomspace.c $(READ_CONFIG) stderrlog.c \
	version-info.c misc-types.c fsusage.h fsusage.c $(GETOPT)


dbck_SOURCES = $(DBCK) \
	dbck-cache.h getopt.h standalone.c \
	misc-types.c stderrlog.c
nodist_dbck_SOURCES = $(NODIST_DBCK)

splitkomdb_SOURCES = splitkomdb.c $(READ_CONFIG) \
	misc-types.c log.c

lyskomd_LDADD = ../libraries/libisc-new/src/libisc.a \
	../libraries/liboop/liboop.a \
	../libraries/adns/src/libadns.a \
	../libraries/libmisc/libmisc.a \
	../libraries/libcommon/liblyskom-server.a \
	../libraries/regex/libregex.a \
	../libraries/libeintr/libeintr.a \
	../libraries/libansi/libansi.a \
	-lm

dbck_LDADD = ../libraries/libmisc/libmisc.a \
	../libraries/libcommon/liblyskom-server.a \
	../libraries/libeintr/libeintr.a \
	../libraries/libansi/libansi.a

LDADD = ../libraries/libmisc/libmisc.a \
	../libraries/libeintr/libeintr.a \
	../libraries/libansi/libansi.a

# Files that implements protocol A.
NODIST_PROTA = prot-a-parse-arg.c
PROTA =  prot-a-output.c prot-a-parse.c prot-a.c \
	 prot-a-send-async.c

# Implementations of the atomic calls.

ATOMS = text.c membership.c person.c conference.c session.c admin.c \
	regex-match.c aux-items.c debug.c

# These files are needed by all versions of the LysKOM server.
NODIST_GENOBJS = $(NODIST_PROTA) aux-no.h
GENOBJS = connections.c log.c $(ATOMS) \
          send-async.c server-config.c text-garb.c \
          isc-parse.c memory.c $(PROTA) \
	  internal-connections.c rfc931.c isc-malloc.c \
	  conf-file.c local-to-global.c server-time.c


# Files for lyskomd.

NODIST_DISKOBJS = version-info.c
DISKOBJS = ramkomd.c ram-smalloc.c simple-cache.c ram-parse.c ram-output.c \
	disk-end-of-atomic.c cache-node.c string-malloc.c

# Files for dbck.

GETOPT = getopt.c getopt1.c
NODIST_DBCK = version-info.c
DBCK = dbck.c dbck-cache.c ram-smalloc.c ram-parse.c server-config.c \
	ram-io.c ram-output.c memory.c conf-file.c $(GETOPT) \
	local-to-global.c lockdb.h lockdb.c server-time.c

# Files for encrypt (a program to transform the database from unencrypted
# apasswords to encrypted).  No longer supported.

ENCRYPT = encrypt-passwd.c dbck-cache.c ram-smalloc.c ram-parse.c \
	server-config.c\
	ram-output.c memory.c

BUILT_SOURCES = prot-a-parse-arg.c version-info.c \
	call-switch.incl com.h fnc-def-init.incl \
	prot-a-parse-arg.h fncdef-no-str-limit.txt .gdbinit \
	version.incl prot-a-is-legal-fnc.incl aux-no.h paths.h

call-switch.incl: call-switch.awk fncdef-no-str-limit.txt
	$(AWK) -f $(srcdir)/call-switch.awk fncdef-no-str-limit.txt \
		> call-switch.incl

com.h: com-h.awk fncdef-no-str-limit.txt
	$(AWK) -f $(srcdir)/com-h.awk fncdef-no-str-limit.txt > com.h

fnc-def-init.incl: fnc-def-init.awk fncdef-no-str-limit.txt
	$(AWK) -f $(srcdir)/fnc-def-init.awk fncdef-no-str-limit.txt \
		> fnc-def-init.incl

prot-a-is-legal-fnc.incl: fncdef-no-str-limit.txt prot-a-is-legal-fnc.awk
	$(AWK) -f $(srcdir)/prot-a-is-legal-fnc.awk fncdef-no-str-limit.txt \
		> prot-a-is-legal-fnc.incl

prot-a-parse-arg.c: prot-a-parse-arg-c.awk fncdef.txt prot-a-parse-arg.h
	$(AWK) -f $(srcdir)/prot-a-parse-arg-c.awk $(srcdir)/fncdef.txt \
		> prot-a-parse-arg.c

prot-a-parse-arg.h: prot-a-parse-arg-h.awk fncdef-no-str-limit.txt
	$(AWK) -f $(srcdir)/prot-a-parse-arg-h.awk \
		fncdef-no-str-limit.txt > prot-a-parse-arg.h

fncdef-no-str-limit.txt: fncdef.txt
	(echo \# Do not edit this file! It is generated from fncdef.txt.;\
	  cat $(srcdir)/fncdef.txt) \
	| sed 's/([^)]*)//g' > fncdef-no-str-limit.txt

version.incl: $(top_srcdir)/versions
	echo '/* Do not edit -- automatically generated file */'      > $@.tmp
	sed -n 's/SERVER-COMPAT-VERSION: //p' $(top_srcdir)/versions  >>$@.tmp
	echo '/* Do not edit -- automatically generated file */'      >>$@.tmp
	mv $@.tmp $@

version-info.c: $(top_srcdir)/versions
	echo '/* Do not edit -- automatically generated file */'   > $@.tmp
	echo '#ifdef HAVE_CONFIG_H'                                >>$@.tmp
	echo '#  include <config.h>'                               >>$@.tmp
	echo '#endif'                                              >>$@.tmp
	echo '#include <limits.h>'                                 >>$@.tmp
	echo '#include <sys/types.h>'                              >>$@.tmp
	echo '#include "misc-types.h"'                             >>$@.tmp
	echo '#include "s-string.h"'                               >>$@.tmp
	echo '#include "kom-types.h"'                              >>$@.tmp
	echo '#include "version-info.h"'                           >>$@.tmp
	echo 'const Version_info_internal kom_version_info = {'    >>$@.tmp
	sed -n 's/PROTOCOL-A-LEVEL: \(.*\)/    \1,/p' \
		$(top_srcdir)/versions                               >>$@.tmp
	sed -n 's/SERVER-SOFTWARE: \(.*\)/    "\1",/p' \
		$(top_srcdir)/versions                               >>$@.tmp
	sed -n 's/SERVER-VERSION: \(.*\)/    "\1"/p' \
		$(top_srcdir)/versions                               >>$@.tmp
	echo '};'                                                  >>$@.tmp
	echo '/* Do not edit -- automatically generated file */'   >>$@.tmp
	mv $@.tmp $@

aux-no.h: $(top_srcdir)/run-support/aux-items.conf Makefile
	$(RM) $@ $@.tmp
	echo '/* Do not edit -- automatically generated file */'    >$@.tmp
	echo '#ifndef AUX_H_INCLUDED'				    >>$@.tmp
	echo '#define AUX_H_INCLUDED'				    >>$@.tmp
	echo 'enum aux_no {'					    >>$@.tmp
	LC_ALL=C LC_CTYPE=C LANG=C sed \
	    -e '/^#/d' \
	    -e '/^{/,/^}/d' \
	    -e 's/^\([0-9]*\) : \([a-z\-]*\) .*/aux_\2 \1/' \
	    -e '/^ *$$/d' \
	    -e 'y/-/_/' \
	    < $(top_srcdir)/run-support/aux-items.conf \
	| awk '{printf "    %-25s = %5s,\n", $$1, $$2}'		    >>$@.tmp
	echo '};'						    >>$@.tmp
	echo '#endif'						    >>$@.tmp
	echo '/* Do not edit -- automatically generated file */'    >>$@.tmp
	chmod 444 $@.tmp
	mv -f $@.tmp $@


.gdbinit: Makefile
	$(RM) .gdbinit
	echo handle 13 nostop noprint 			>.gdbinit
	echo dir $(top_srcdir)/src/libraries/libcommon	>>.gdbinit
	echo dir $(top_srcdir)/src/libraries/libansi	>>.gdbinit
	echo dir $(top_srcdir)/src/libraries/libisc-new	>>.gdbinit
	echo dir $(top_srcdir)/src/libraries/liboop	>>.gdbinit
	echo dir $(top_srcdir)/src/libraries/libmisc	>>.gdbinit

DEFP = $(top_srcdir)/scripts/definepath "$(top_srcdir)" "$(prefix)"

paths.h: $(top_srcdir)/scripts/unprefix $(top_srcdir)/scripts/definepath \
		Makefile
	$(RM) $@ $@.tmp
	echo '/* Do not edit -- automatically generated file */'    >  $@.tmp
	echo '#define DEFAULT_PREFIX "'"$(prefix)"'"'               >> $@.tmp
	$(DEFP) CONFIG_FILE      "$(sysconfdir)/lyskomd.conf"       >> $@.tmp
	$(DEFP) DATA_FILE        "$(dbdir)/lyskomd-data"	    >> $@.tmp
	$(DEFP) BACKUP_FILE      "$(dbdir)/lyskomd-backup"	    >> $@.tmp
	$(DEFP) PREV_BACKUP_FILE "$(dbdir)/lyskomd-backup-prev"	    >> $@.tmp
	$(DEFP) LOCK_FILE        "$(dbdir)/lyskomd-lock"	    >> $@.tmp
	$(DEFP) TEXT_FILE        "$(dbdir)/lyskomd-texts"	    >> $@.tmp
	$(DEFP) NUMBER_FILE      "$(dbdir)/number.txt"		    >> $@.tmp
	$(DEFP) NUMBER_FILE_TMP  "$(dbdir)/number.tmp"		    >> $@.tmp
	$(DEFP) TEXT_BACKUP_FILE "$(dbdir)/lyskomd-texts-backup"    >> $@.tmp
	$(DEFP) EXPORT_DIR       "$(exportdir)"	 		    >> $@.tmp
	$(DEFP) LYSKOMD_LOG      "$(localstatedir)/lyskomd.log"     >> $@.tmp
	$(DEFP) LYSKOMD_STATS    "$(localstatedir)/lyskomd.stats"   >> $@.tmp
	$(DEFP) LYSKOMD_PID      "$(localstatedir)/run/lyskomd.pid" >> $@.tmp
	$(DEFP) MEMORY_USAGE     "$(localstatedir)/lyskomd.memory"  >> $@.tmp
	$(DEFP) AUX_FILE         "$(sysconfdir)/aux-items.conf"     >> $@.tmp
	$(DEFP) STATUS_FILE      "$(dbdir)/status"		    >> $@.tmp
	$(DEFP) CONNECTIONS_FILE "$(localstatedir)/lyskomd.clients" >> $@.tmp
	$(DEFP) CONNECTIONS_TMP  "$(localstatedir)/lyskomd.clnt.tmp">> $@.tmp
	$(DEFP) CORE_DIR         "$(localstatedir)/lyskomd.cores"   >> $@.tmp
	$(DEFP) LYSKOMD_PATH     "$(sbindir)/lyskomd$(EXEEXT)"      >> $@.tmp
	$(DEFP) SAVECORE_PATH "$(sbindir)/savecore-lyskom$(EXEEXT)" >> $@.tmp
	echo "/* External programs */"                              >> $@.tmp
	$(DEFP) SENDMAIL_PATH    "$(SENDMAIL)"                      >> $@.tmp
	echo '/* Do not edit -- automatically generated file */'    >> $@.tmp
	chmod 444 $@.tmp
	mv -f $@.tmp $@

check-lyskomd: libcheck.a lyskomd dbck $(CHECKKOMSPACE)
	cd testsuite && $(MAKE) check-lyskomd

check-leaks: libcheck.a lyskomd dbck
	cd testsuite && $(MAKE) check-leaks

check-l2g: libcheck.a
	cd testsuite && $(MAKE) check-l2g

# Make sure these gets recompiled if $(prefix) changes.
server-config.o: Makefile
updateLysKOM.o: Makefile

# After a "make clean" followed by "make check", these are needed.
aux-items.o: aux-no.h
text.o: aux-no.h

admin.o: com.h
aux-item-def-parse.o: com.h
aux-items.o: com.h
conference.o: com.h
connections.o: com.h
dbck.o: com.h
debug.o: com.h
internal-connections.o: com.h
isc-parse.o: com.h
membership.o: com.h
person.o: com.h
prot-a-output.o: com.h
prot-a-parse-arg.o: com.h
prot-a-parse.o: com.h
prot-a-send-async.o: com.h
prot-a.o: com.h
ramkomd.o: com.h
regex-match.o: com.h
send-async.o: com.h
server-config.o: com.h
session.o: com.h
simple-cache.o: com.h
standalone.o: com.h
text-garb.o: com.h
text.o: com.h
server-config.o: paths.h

connections.o: fnc-def-init.incl call-switch.incl
admin.o: version.incl
prot-a.o: fnc-def-init.incl prot-a-is-legal-fnc.incl
standalone.o: version.incl

# "make install" without a prior "make" requires these.
connections.o: prot-a-parse-arg.h

# This dependency is needed after a "make maintainer-clean".
aux-item-def-scan.o: aux-item-def-parse.h

# 
all-recursive: lyskomd$(EXEEXT) dbck$(EXEEXT)
check-recursive: libcheck.a lyskomd$(EXEEXT) dbck$(EXEEXT) $(CHECKKOMSPACE)
