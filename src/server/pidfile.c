/*
 * $Id: pidfile.c,v 1.5 2003/08/23 16:38:15 ceder Exp $
 * Copyright (C) 1999, 2002-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include "pidfile.h"
#include "eintr.h"

pid_t
read_pid_file(const char *pidfile,
	      const char *arg0)
{
    FILE *fp;
    int saved_errno;
    pid_t pid = 0;
    int c;

    fp = i_fopen(pidfile, "r");
    if (fp == NULL)
    {
	saved_errno = errno;
	fprintf(stderr, "%s: warning: ", arg0);
	errno = saved_errno;
	perror(pidfile);
    }
    else
    {
	while ((c = getc(fp)) != EOF && c == ' ')
	    ;
	ungetc(c, fp);
	while ((c = getc(fp)) != EOF && c >= '0' && c <= '9')
	    pid = 10 * pid + c - '0';
	i_fclose(fp);
	if (pid < 2)
	{
	    fprintf(stderr, "%s: got pid %ld.\n", arg0, (long)pid);
	    exit(1);
	}
    }

    return pid;
}
