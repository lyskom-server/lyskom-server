/*
 * $Id: strerror.c,v 0.24 2003/08/23 16:38:20 ceder Exp $
 * Copyright (C) 1991-1995, 1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
** strerror.c
*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#ifdef HAVE_STRING_H
#  include <string.h>
#endif

extern int sys_nerr;
extern const char *sys_errlist[];

char *strerror(int eno)
{
  static char buf[20];

  if (eno < 0 || eno >= sys_nerr)
  {
    sprintf(buf, "Error %d", eno);
    return buf;
  }
  else
    return (char*)sys_errlist[eno];
}
