/*
 * $Id: misc-parser.c,v 0.21 2003/08/23 16:38:20 ceder Exp $
 * Copyright (C) 1990-1991, 1993-1995, 1998-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 *  misc-parser.c
 *		Parse a list of misc-items for LysKOM texts.
 *
 *
 *  Copyright (C) 1990-1991, 1993-1995, 1998-1999, 2001-2003	Lysator Computer Club,
 *			Linkoping University,  Sweden
 *
 *  Everyone is granted permission to copy, modify and redistribute
 *  this code, provided the people they give it to can.
 *
 *
 *  Author:	Thomas Bellman
 *		Lysator Computer Club
 *		Linkoping University
 *		Sweden
 *
 *  email:	Bellman@Lysator.LiU.SE
 *
 *
 *  Any opinions expressed in this code are the author's PERSONAL opinions,
 *  and does NOT, repeat NOT, represent any official standpoint of Lysator,
 *  even if so stated.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <time.h>

#include "s-string.h"
#include "misc-types.h"
#include "misc-parser.h"

#define EXPORT


/*
 *  Return TRUE if the next misc-item exist, and it is of type
 *  MTYPE, FALSE otherwise.  Can only be used within the function
 *  parse_next_misc().
 */
#define NEXT_IS(mtype)						\
    (*info < stop_pointer && (*info)->type == (mtype))


#define BARF	{  result.type = m_error;  return  result;  }


/*
 *  Parse out a "group" of misc-items from a list of them pointed
 *  to by *INFO.  *STOP_POINTER must point to the item directly
 *  after the last in the list.  **INFO will be incremented to
 *  point to the item after the recently parsed out group.  The
 *  return value will have the TYPE field set to 'm_end_of_list'
 *  when the end of the list has been reached.
 */
EXPORT  Misc_info_group
parse_next_misc (const Misc_info	** info,
		 const Misc_info	 * stop_pointer)

{
    Misc_info_group	  result;

    
	
    /* Clear the 'result' struct */
    result.type = m_error;
    result.recipient = 0;
    result.cc_recipient = 0;
    result.bcc_recipient = 0;
    result.local_no = 0;
    result.received_at = NO_TIME;
    result.comment_to = 0;
    result.commented_in = 0;
    result.footnote_to = 0;
    result.footnoted_in = 0;
    result.sender = 0;
    result.sent_at = NO_TIME;
    result.is_received = FALSE;
    result.is_sent = FALSE;


    if (*info >= stop_pointer)
    {
	result.type = m_end_of_list;
	return  result;
    }

    
    /* Now, do the real work... */
    switch ((*info)->type)
    {
    case  recpt:	/* These two are so similar that they can */
    case  cc_recpt:	/* Be handled in the same clause	  */
    case  bcc_recpt:
	
	if ((*info)->type == recpt)
	{
	    result.type = m_recpt;
	    result.recipient = (*(*info)++).datum.recipient;
	}
	else if ((*info)->type == cc_recpt)
	{
	    result.type = m_cc_recpt;
	    result.cc_recipient = (*(*info)++).datum.recipient;
	}
	else
	{
	    result.type = m_bcc_recpt;
	    result.bcc_recipient = (*(*info)++).datum.recipient;
	}
	
	/* There should follow a 'Local no', but check nevertheless */
	if (! NEXT_IS (loc_no))
	    { BARF }
	else
	    result.local_no = (*(*info)++).datum.local_no;

	if (NEXT_IS (rec_time))
	{
	    result.is_received = TRUE;
	    result.received_at = (*(*info)++).datum.received_at;
	}

	if (NEXT_IS (sent_by))
	{
	    result.sender = (*(*info)++).datum.sender;
	    /* There _should_ be a 'sent_at' here... */
	    if (! NEXT_IS (sent_at))
		{ BARF }
	    /* Let the following if clause insert the 'sent_at' data. */
	}
	if (NEXT_IS (sent_at))
	{
	    result.is_sent = TRUE;
	    result.sent_at = (*(*info)++).datum.sent_at;
	}

	break;


    case  comm_to:
	result.type = m_comm_to;
	result.comment_to = (*(*info)++).datum.text_link;

	if (NEXT_IS (sent_by))
	{
	    result.sender = (*(*info)++).datum.sender;
	    /* There _should_ be a 'sent_at' here. */
	    if (! NEXT_IS (sent_at))
		{ BARF }
	    /* Let the following if clause insert the 'sent_at' data. */
	}
	if (NEXT_IS (sent_at))
	{
	    result.is_sent = TRUE;
	    result.sent_at = (*(*info)++).datum.sent_at;
	}

	break;


    case  footn_to:
	result.type = m_footn_to;
	result.footnote_to = (*(*info)++).datum.text_link;

	if (NEXT_IS (sent_at))
	{
	    result.is_sent = TRUE;
	    result.sent_at = (*(*info)++).datum.sent_at;
	}

	break;


    case  comm_in:
	result.type = m_comm_in;
	result.commented_in = (*(*info)++).datum.text_link;
	break;


    case  footn_in:
	result.type = m_footn_in;
	result.footnoted_in = (*(*info)++).datum.text_link;
	break;


#ifndef COMPILE_CHECKS
    default:
#endif
    case unknown_info:
    case loc_no:
    case rec_time:
    case sent_by:
    case sent_at:
	result.type = m_error;
	break;
    }

    return  result;
}
