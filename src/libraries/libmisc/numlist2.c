/*
 * $Id: numlist2.c,v 0.16 2003/08/23 16:38:19 ceder Exp $
 * Copyright (C) 1991, 1993-1995, 1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * File: numlist2.c
 * 
 * This file is supposed to be a reference while testing numlist.
 *
 * Author: Inge Wallin
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif

#include "misc-types.h"

#include "numlist2.h"


Numlist *
Numlist_create()
{
    Numlist  * nl;
    int        i;

    nl = (Numlist *) malloc(sizeof(Numlist));
    if (nl == NULL) {
	fprintf(stderr, "Virtual memory exhausted");
	abort();
    }

    for (i = 0; i < NUM_CELLS; ++i)
	nl->cells[i] = 0;
    
    return nl;
}


Bool
numlist_isempty(Numlist *nl)
{
    int   i;

    for (i = 0; i < NUM_CELLS; ++i)
	if (nl->cells[i] != 0)
	    return FALSE;
    
    return TRUE;
}


Bool
numlist_member(Numlist *nl, u_long num)
{
    return (nl->cells[num / 32] & ((u_long) 1 << (num & 31))) != 0;
}


u_long
numlist_first(Numlist *nl)
{
    int   i;
    int   j;

    if (numlist_isempty(nl))
	return NUMLIST_ERR;

    for (i = 0; i < NUM_CELLS; ++i) {
	if (nl->cells[i] != 0) {
	    for (j = 31; j >= 0; --j) {
		if ((nl->cells[i] & ((u_long) 1 << j)) != 0)
		    return i * 32 + j;
	    }
	}
    }
    
    return NUMLIST_ERR;
}


u_long
numlist_next(Numlist *nl, u_long num)
{
    /* NYI */
    return NUMLIST_ERR;
}


void
numlist_insert(Numlist *nl, u_long num)
{
    nl->cells[num / 32] |= ((u_long) 1 << (num & 31));
}


void
numlist_delete(Numlist *nl, u_long num)
{
    nl->cells[num / 32] &= ~((u_long) 1 << (num & 31));
}

