/*
 * $Id: numlist.c,v 0.17 2003/08/23 16:38:19 ceder Exp $
 * Copyright (C) 1991, 1993-1995, 1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * File: numlist.c
 * 
 * A numlist is supposed to be an efficient implementation of 
 * a list of numbers. One of the big features is intervals.
 *
 * Author: Inge Wallin
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif

#include "misc-types.h"

#include "numlist.h"


/* ================================================================ */
/*                        Some useful macros                        */


#define IS_INTERVAL_START(x)  (((x) & INTERVAL_MASK) == INTERVAL_START)
#define IS_INTERVAL_END(x)    (((x) & INTERVAL_MASK) == INTERVAL_END)

#define NUMBER_OF(x)          ((x) & NUMBER_MASK)

#define MAKE_INTERVAL_START(x)  ((x) | INTERVAL_START)
#define MAKE_INTERVAL_END(x)    ((x) | INTERVAL_END)


/* ================================================================ */
/*                           Numlist_node                           */


static Numlist_node *
numlist_node_create()
{
    Numlist_node  * nln;

    nln = (Numlist_node *) malloc(sizeof(Numlist_node));
    if (nln == NULL) {
	fprintf(stderr, "Virtual memory exhausted");
	abort();
    }

    nln->num_numbers = 0;
    nln->next = NULL;
    
    return nln;
}



/*
 * Insert the number NUM at index INDEX in the numlist node NLN.
 * All numbers starting at index INDEX and subsequent are moved
 * one step back.
 */

static void
numlist_node_insert_num(Numlist_node  * nln,
			int             index,
			u_long          num)
{
    int   i;

    for (i = nln->num_numbers; i > index; --i)
	nln->numbers[i] = nln->numbers[i - 1];
    nln->numbers[index] = num;
    ++nln->num_numbers;
}



/*
 * Delete the number with index INDEX from the node NODE. If this
 * was the last number in the node, delete the entire node. PREV_NODE
 * points to the node just before NODE and NL is the Numlist they
 * both belong to.
 */

static void
numlist_node_del_num(Numlist       * nl,
		     Numlist_node  * node,
		     Numlist_node  * prev_node,
		     int             index)
{
    int   i;

    /* Delete the number from this node. */
    for (i = index; i < node->num_numbers - 1; ++i)
	node->numbers[i] = node->numbers[i + 1];
    --(node->num_numbers);

    /* If this node became empty, delete it and fix all pointers. */
    if (node->num_numbers == 0) {
	if (prev_node == NULL) {
	    nl->first = node->next;
	    if (nl->first == NULL)
		nl->last = NULL;
	} else {
	    prev_node->next = node->next;
	    if (nl->last == node)
		nl->last = prev_node;
	}
	free(node);
    }
}



/*
 * Split the node NODE into two nodes.  The numbers starting with the
 * index INDEX is put in the second (new) node and the other remain
 * in the old one.  NL points to the Numlist the node belongs to.
 */

static void
numlist_node_split(Numlist       * nl,
		   Numlist_node  * node,
		   int             index)
{
    Numlist_node  * new_node;
    int             i;

    new_node = numlist_node_create();
    new_node->next = node->next;
    node->next = new_node;

    for (i = index; i < node->num_numbers; ++i)
	new_node->numbers[i - index] = node->numbers[i];

    new_node->num_numbers = node->num_numbers - index;
    node->num_numbers = index;

    if (nl->last == node)
	nl->last = new_node;
}


/* ================================================================ */
/*                              Numlist                             */


/*
 * Create a new, empty Numlist and return a pointer to it.
 */

Numlist *
numlist_create()
{
    Numlist  * nl;

    nl = (Numlist *) malloc(sizeof(Numlist));
    if (nl == NULL) {
	fprintf(stderr, "Virtual memory exhausted");
	abort();
    }

    nl->first = NULL;
    nl->last = NULL;
    nl->last_num = NUMLIST_NO_NUMBER;
    
    return nl;
}



/*
 * Return TRUE if the Numlist NL is empty, otherwise return FALSE.
 */

Bool
numlist_isempty(Numlist *nl)
{
    return nl->first == NULL;
}



/*
 * return TRUE if NUM is a member of the numlist NL, otherwise
 * return FALSE.
 */

Bool
numlist_member(Numlist *nl, u_long num)
{
    Numlist_node  * nln;
    u_long          last;
    int             i;

    /* If num is bigger than the last number, return FALSE immediately. */
    last = numlist_last(nl);
    if (last == NUMLIST_ERR || num > last)
	return FALSE;

    nln = nl->first;
    while (nln != NULL) {
	if (num <= NUMBER_OF(nln->numbers[nln->num_numbers - 1])) {
	    for (i = 0; i < nln->num_numbers; ++i) {

		/* Check if we can abort the loop. */
		if (num < NUMBER_OF(nln->numbers[i]))
		    return FALSE;

		/* Take care of an interval. */
		if (IS_INTERVAL_START(nln->numbers[i])) {
		    if (NUMBER_OF(nln->numbers[i]) <= num
			&& num <= NUMBER_OF(nln->numbers[i + 1]))
			return TRUE;
		    ++i;
		} else {
		    if (nln->numbers[i] == num)
			return TRUE;
		}
	    }
	}

	nln = nln->next;
    }
    return FALSE;
}



/*
 * Return the first number in the numlist NL. Also store a pointer
 * to the first node, the index to this particular cell and the
 * first number itself in the numlist. This makes it more efficient
 * to get the next number later.
 *
 * If NL is empty, return the constant NUMLIST_ERR.
 */

u_long
numlist_first(Numlist *nl)
{
    u_long   first;

    if (numlist_isempty(nl)) {
	return NUMLIST_ERR;
    } else {
	first = NUMBER_OF(nl->first->numbers[0]);
	nl->last_num = first;
	nl->last_node = nl->first;
	nl->last_index = 0;

	return first;
    }
}



/*
 * Return the last number in the numlist NL.  If NL is empty, 
 * return the constant NUMLIST_ERR.
 * 
 * Unlike numlist_first(), this routine does NOT set last_num 
 * and the other cache fields in NL.
 */

u_long
numlist_last(Numlist *nl)
{
    Numlist_node  * last;

    if (numlist_isempty(nl)) {
	return NUMLIST_ERR;
    } else {
	last = nl->last;
	return NUMBER_OF(last->numbers[last->num_numbers - 1]);
    }
}



/*
 * Return the first number that is a member of the numlist NL and
 * bigger than NUM. If there is no such number, the constant
 * NUMLIST_ERR is returned.
 */

u_long
numlist_next(Numlist *nl, u_long num)
{    
    Numlist_node  * nln;
    int             index;
    int             i;

    /* If we got NUM the last time, we have cached some info about */
    /* where to find the next entry. Use that info. */
    if (num == nl->last_num) {
	nln = nl->last_node;
	index = nl->last_index;
	if (IS_INTERVAL_START(nln->numbers[index])) {

	    /* Last number was in an interval. */
	    if (num < NUMBER_OF(nln->numbers[index + 1])) {
		nl->last_num = num + 1;
		return num + 1;
	    } else {
		++index;
	    }
	} 

	++index;
	if (index < nln->num_numbers) {
	    nl->last_num = NUMBER_OF(nln->numbers[index]);
	    nl->last_index = index;
	    return nl->last_num;
	}

	/* If we get here the next number is not in this node */
	/* but in the next. */
	nln = nln->next;
	if (nln == NULL) {
	    nl->last_num = NUMLIST_NO_NUMBER;
	    return NUMLIST_ERR;
	} else {
	    nl->last_num = NUMBER_OF(nln->numbers[0]);
	    nl->last_node = nln;
	    nl->last_index = 0;

	    return nl->last_num;
	}
    }
	
    /* ...otherwise find NUM in the list and return the next number.  */
    nln = nl->first;
    ++num;
    while (nln != NULL) {
	if (num <= NUMBER_OF(nln->numbers[nln->num_numbers - 1])) {
	    for (i = 0; i < nln->num_numbers; ++i) {

		if (num <= NUMBER_OF(nln->numbers[i])) {
		    nl->last_num = NUMBER_OF(nln->numbers[i]);
		    nl->last_node = nln;
		    nl->last_index = i;
		    return nl->last_num;
		}

		/* Take care of an interval. */
		if (IS_INTERVAL_START(nln->numbers[i])) {
		    if (num <= NUMBER_OF(nln->numbers[i + 1])) {
			nl->last_num = num;
			nl->last_node = nln;
			nl->last_index = i;

			return num;
		    } else {
			++i;
		    }
		}
	    }
	}
	nln = nln->next;
    }

    return NUMLIST_ERR;
}



/*
 * Enter the number NUM into the numlist NL. If NL is already in
 * NL, do nothing.
 */

void
numlist_insert(Numlist *nl, u_long num)
{
    Numlist_node  * nln;
    Numlist_node  * last;
    int             last_num;
    int             i;
    int             j;

    nl->last_num = NUMLIST_NO_NUMBER;

    last_num = numlist_last(nl);
    if (last_num != NUMLIST_ERR && num <= last_num) {
	nln = nl->first;
	while (nln != NULL) {
	    if (num <= NUMBER_OF(nln->numbers[nln->num_numbers - 1])) {
		for (i = 0; i < nln->num_numbers; ++i) {
		    if (num <= NUMBER_OF(nln->numbers[i]))
			goto found;
		}
	    }
	    nln = nln->next;
	}
    }
    
    /* If we get here, num is not found. It is also the case that num */
    /* is bigger than all numbers in the numlist. */
    last = nl->last;
    if (last == NULL) {

	/* Empty numlist. */
	nl->first = nl->last = numlist_node_create();
	nl->first->num_numbers = 1;
	nl->first->numbers[0] = num;

    } else if ((NUMBER_OF(last->numbers[last->num_numbers - 1]) == num - 1)
	       && IS_INTERVAL_END(last->numbers[last->num_numbers - 1])) {
	/* Enter num into last interval */
	last->numbers[last->num_numbers - 1] = MAKE_INTERVAL_END(num);
    
    } else if (last->num_numbers < NUM_LONGS_IN_NUMLIST) {
	/* There is room for num in the last Numlist_node. */
	if (last->numbers[last->num_numbers - 1] == num - 1) {
	    last->numbers[last->num_numbers - 1]
		= MAKE_INTERVAL_START(num - 1);
	    last->numbers[last->num_numbers++] = MAKE_INTERVAL_END(num);
	} else
	    last->numbers[last->num_numbers++] = num;
	
	
    } else {
	/* We have to create a new Numlist_node to host num. */
	nl->last = numlist_node_create();
	last->next = nl->last;
	nl->last->num_numbers = 1;
	nl->last->numbers[0] = num;
    }
    return;

  found:
    /* Here we have found a number that is bigger than num. */
    /* nln points to the Numlist_node and i is the index to it. */
    
    /* If num is already in the list, do nothing. */
    if (NUMBER_OF(nln->numbers[i]) == num
	|| IS_INTERVAL_END(nln->numbers[i]))
	return;

    /* Step through the different cases and take appropriate action. */
    if (IS_INTERVAL_START(nln->numbers[i])
	&& NUMBER_OF(nln->numbers[i]) == num + 1) {

	/* Enter num into the next interval. */
	nln->numbers[i] = MAKE_INTERVAL_START(num);

	/* FIXME: Take care of the case that two intervals could be */
	/*    collapsed into one, e.g. like 17-21, 22-31 => 17-31. */

    } else if (i > 0 
	       && IS_INTERVAL_END(nln->numbers[i - 1])
	       && NUMBER_OF(nln->numbers[i - 1]) == num - 1) {

	/* Enter num into the previous interval. */
	nln->numbers[i - 1] = MAKE_INTERVAL_END(num);

    } else if (nln->num_numbers < NUM_LONGS_IN_NUMLIST) {

	/* Squeeze in num. There is room in this Numlist_node. */
	for (j = nln->num_numbers; j > i; --j)
	    nln->numbers[j] = nln->numbers[j - 1];
	if (i > 0 && nln->numbers[i - 1] == num - 1) {
	    nln->numbers[i - 1] = MAKE_INTERVAL_START(num - 1);
	    nln->numbers[i] = MAKE_INTERVAL_END(num);
	} else if (i < nln->num_numbers - 1 
		   && nln->numbers[i + 1] == num + 1) {
	    nln->numbers[i] = MAKE_INTERVAL_START(num);
	    nln->numbers[i + 1] = MAKE_INTERVAL_END(num + 1);
	} else {
	    nln->numbers[i] = num;
	}

	nln->num_numbers++;

    } else {
	/* Squeeze in num. There is no room in this Numlist_node. */
	/* FIXME: This implementation is a bit crude. We should split */
	/*    the node as evenly as possible. */

	numlist_node_split(nl, nln, i);
	nln->numbers[nln->num_numbers++] = num;
    }

    /* FIXME: We never take care of the case that two intervals in adjacent */
    /*    nodes could be collapsed into one. Should we? */
    return;
}



/*
 * Delete the number NUM from the Numlist NL. If NUM is not
 * a member of NL, do nothing.
 */

void
numlist_delete(Numlist *nl, u_long num)
{
    Numlist_node  * nln1;
    Numlist_node  * nln2;
    int             last_num;
    int             i;
    int             j;

    nl->last_num = NUMLIST_NO_NUMBER;

    last_num = numlist_last(nl);
    if (last_num == NUMLIST_ERR || num > last_num)
	return;

    nln1 = nl->first;
    nln2 = NULL;
    while (nln1 != NULL) {
	if (num <= NUMBER_OF(nln1->numbers[nln1->num_numbers - 1])) {
	    for (i = 0; i < nln1->num_numbers; ++i) {
		if (num == nln1->numbers[i] /* An ordinary number */
		    || (IS_INTERVAL_START(nln1->numbers[i])
			&& NUMBER_OF(nln1->numbers[i]) <= num
			&& num <= NUMBER_OF(nln1->numbers[++i])))
		    goto found;

		/* If we are already past the number, it is not here */
		/*  ==> return. */
		if (num < NUMBER_OF(nln1->numbers[i]))
		    return;
	    }
	}
	nln2 = nln1;
	nln1 = nln1->next;
    }
    return;
    
    /* When we get here, nln1 points to the node where the number */
    /* resides, nln2 points to the node before if not NULL, and i */
    /* i is the index into the node. If the number is within an */
    /* interval, i points to the end of the interval (++i above). */
  found:
    if ((nln1->numbers[i] & INTERVAL_MASK) == 0) {
	/* An ordinary number. */
	numlist_node_del_num(nl, nln1, nln2, i);
	
    } else {
	/* An interval. */
	if (NUMBER_OF(nln1->numbers[i]) == num) {
	    /* The number is at the end of the interval. */
	    --(nln1->numbers[i]);
	    if (NUMBER_OF(nln1->numbers[i])
		== NUMBER_OF(nln1->numbers[i - 1])) {

		numlist_node_del_num(nl, nln1, nln2, i);
		nln1->numbers[i - 1] &= NUMBER_MASK;
	    }

	} else if (NUMBER_OF(nln1->numbers[i - 1]) == num) {
	    /* The number is at the start of the interval. */
	    ++(nln1->numbers[i - 1]);
	    if (NUMBER_OF(nln1->numbers[i])
		== NUMBER_OF(nln1->numbers[i - 1])) {

		numlist_node_del_num(nl, nln1, nln2, i);
		nln1->numbers[i - 1] &= NUMBER_MASK;
	    }
	    
	} else if (NUMBER_OF(nln1->numbers[i - 1]) == num - 1
		   && NUMBER_OF(nln1->numbers[i]) == num + 1) {
	    /* The interval is only 3 numbers */
	    nln1->numbers[i - 1] &= NUMBER_MASK;
	    nln1->numbers[i] &= NUMBER_MASK;

	} else {
	    /* The number is within the interval. We must split it. */
	    if (IS_INTERVAL_END(nln1->numbers[i]))
		--i;

	    /* If there is not room for two new numbers, split the node. */
	    if (nln1->num_numbers > NUM_LONGS_IN_NUMLIST - 2) {
		if (i < NUM_LONGS_IN_NUMLIST / 2) {	
		    /* Split the node after the interval and keep it here. */
		    numlist_node_split(nl, nln1, i + 2);
		} else {
		    /* Split the node before the interval */
		    /* and use the new node. */
		    numlist_node_split(nl, nln1, i);
		    nln1 = nln1->next;
		    i = 0;
		}
	    }

	    /* Now nln1 points to the node with the interval and */
	    /* i is the index to the start of the interval. */
	    if (NUMBER_OF(nln1->numbers[i]) == num - 1) {
		/* NUM is second element ==> leave only one to the left. */
		numlist_node_insert_num(nln1, i,
					NUMBER_OF(nln1->numbers[i]));
		nln1->numbers[i + 1] += 2;

	    } else if (NUMBER_OF(nln1->numbers[i + 1]) == num + 1) {
		/* NUM is second last element ==> leave only one to */
		/* the right. */
		numlist_node_insert_num(nln1, i + 2,
					NUMBER_OF(nln1->numbers[i + 1]));
		nln1->numbers[i + 1] -= 2;

	    } else {
		/* NUM is deep inside the interval */
		/*   ==> Make 2 new intervals. */
		for (j = nln1->num_numbers + 1; j >= i + 2; --j)
		    nln1->numbers[j] = nln1->numbers[j - 2];
		nln1->numbers[i + 1] = MAKE_INTERVAL_END(num - 1);
		nln1->numbers[i + 2] = MAKE_INTERVAL_START(num + 1);
		nln1->num_numbers += 2;
	    }
	}
    }
}
