/*
 * $Id: testnumlist.c,v 0.15 2003/08/23 16:38:19 ceder Exp $
 * Copyright (C) 1991, 1993-1995, 1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif

#include <misc-types.h>

#include "numlist.h"


/* ================================================================ */
/*                        Some useful macros                        */


#define IS_INTERVAL_START(x)  (((x) & INTERVAL_MASK) == INTERVAL_START)
#define IS_INTERVAL_END(x)    (((x) & INTERVAL_MASK) == INTERVAL_END)

#define NUMBER_OF(x)          ((x) & NUMBER_MASK)

#define MAKE_INTERVAL_START(x)  ((x) | INTERVAL_START)
#define MAKE_INTERVAL_END(x)    ((x) | INTERVAL_END)


/* ================================================================ */


void
numlist_node_print(Numlist_node *nln)
{
    int      i;
    u_long   num;

    putchar('[');
    for (i = 0; i < NUM_LONGS_IN_NUMLIST; ++i) {
	if (i < nln->num_numbers) {
	    num = nln->numbers[i];
	    printf("%c%5U%c,", IS_INTERVAL_START(num) ? '<' : ' ',
		   NUMBER_OF(num), IS_INTERVAL_END(num) ? '>' : ' ');
	} else {
	    printf("       ,");
	}
    }
    putchar(']');
    putchar('\n');
}



void
numlist_print(Numlist *nl)
{
    Numlist_node  * nln;

    printf("last_num   = %U\n", nl->last_num);
    printf("last_index = %U\n", nl->last_index);
    nln = nl->first;
    while (nln != NULL) {
	numlist_node_print(nln);
	nln = nln->next;
    }
    putchar('\n');
}



int
main()
{
    Numlist  * nl;
    Bool       finished;

    nl = numlist_create();
    puts("Empty numlist:");
    numlist_print(nl);

    finished = FALSE;
    while (!finished) {
	char     command;
	u_long   num;

	scanf("\n%c%U", &command, &num);
	switch (command) {
	  case 'i':
	    numlist_insert(nl, num);
	    printf("%U inserted\n", num);
	    numlist_print(nl);
	    break;

	  case 'd':
	    numlist_delete(nl, num);
	    printf("%U deleted\n", num);
	    numlist_print(nl);
	    break;

	  case 'q':
	    finished = TRUE;
	    break;

	  case ' ':
	  case '\n': 
	    break;

	  default:
	    printf("Unrecognized command: %c\n", command);
	    break;
	}
    }
    
    return 0;
}

