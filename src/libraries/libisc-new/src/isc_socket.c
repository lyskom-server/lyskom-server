/*
** isc_socket.c                               Socket specified code
**
** Copyright (C) 1992, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
**   920210 pen     initial coding
** (See ChangeLog for recent history)
*/

#include <stdio.h>
#ifdef TIME_WITH_SYS_TIME
#  include <sys/time.h>
#  include <time.h>
#else
#  ifdef HAVE_SYS_TIME_H
#    include <sys/time.h>
#  else
#    include <time.h>
#  endif
#endif
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <errno.h>
#include <netdb.h>
#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_STRING_H
#  include <string.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#include <fcntl.h>
#include <assert.h>
#include <stdlib.h>

#include "oop.h"
#include "adns.h"
#include "oop-adns.h"

#include "s-string.h"

#include "isc.h"
#include "intern.h"
#include "isc_addr.h"
#include "unused.h"

static oop_adns_call isc_dns_resolve_adns_cb;

#if defined(__GNUC__) && defined(__sparc__)
/*
** inet_ntoa() is buggy on SPARCs running SunOS 4.1.1, so
** we use our own
*/
#ifdef inet_ntoa
#undef inet_ntoa
#endif

char *inet_ntoa(ad)
    struct in_addr ad;
{
  u_long s_ad;
  int a, b, c, d;
  static char addr[20];
  
  s_ad = ad.s_addr;
  d = s_ad % 256;
  s_ad /= 256;
  c = s_ad % 256;
  s_ad /= 256;
  b = s_ad % 256;
  a = s_ad / 256;
  sprintf(addr, "%d.%d.%d.%d", a, b, c, d);
  
  return addr;
}
#endif



union isc_address *
isc_mkipaddress(SOCKADDR_STORAGE *addr)
{
  union isc_address *ia;


  ISC_XNEW(ia);
  memcpy(&ia->saddr, addr, sizeof(ia->saddr));

  return ia;
}

union isc_address *
isc_copyaddress(union isc_address *addr)
{
  union isc_address *new_addr;

  ISC_XNEW(new_addr);
  *new_addr = *addr;

  return new_addr;
}


void
isc_freeaddress(union isc_address *addr)
{
  isc_free(addr);
}


/* isc_getipnum makes a textual representation of the IP address
   stored in an union isc_address. When an IPv6 socket is connected to an
   IPv4 socket this will be an IPv6-mapped IPv4 address. To hide this
   from the user (who might not be used to IPv6), we'll extract the
   IPv4 address part and present it as an ordinary IPv4 address.
   RFC2553 suggests no pretty way to do this, but says that we can get
   the IPv4 address by skipping the first 12 bytes of the in6_addr
   structure -- so that's what we'll do...

   If you have a weird IPv6 implementation that doesn't have inet_ntop
   you simply lose -- RFC2553 requires inet_ntop, and clearly states
   that inet_ntoa is specific to IPv4. However, it's still possible to
   handle IPv6-mapped IPv4 addresses with inet_ntoa and the same
   trickery as above.
*/


char *isc_getipnum(union isc_address *ia, char *buf, int len)
{
  static char hostname[256];
  

  if (!buf)
  {
    buf = hostname;
    len = sizeof(hostname);
  }
  
#ifdef HAVE_INET_NTOP
  CHOOSE_IP4OR6(ia->saddr,
		inet_ntop(ia->saddr.sa.sa_family,
			  (const void *) &ia->saddr.sa_in.sin_addr,
			  buf,
			  len),
		IN6_IS_ADDR_V4MAPPED(&ia->saddr.sa_in6.sin6_addr)
		? inet_ntop(AF_INET,
			    (const void *)
			    &ia->saddr.sa_in6.sin6_addr.s6_addr[12],
			    buf,
			    len)
		: inet_ntop(ia->saddr.sa.sa_family,
			    (const void *) &ia->saddr.sa_in6.sin6_addr,
			    buf,
			    len));
#else
  strncpy(buf,
	  CHOOSE_IP4OR6(ia->saddr,
			inet_ntoa(ia->saddr.sa_in.sin_addr),
			IN6_IS_ADDR_V4MAPPED(&ia->saddr.sa_in6.sin6_addr)
			? inet_ntoa((struct in_addr *)
				    &ia->saddr.sa_in6.sin6_addr.s6_addr[12])
			: ""),
	  len-1);
#endif
  buf[len-1] = '\0';

  return buf;
}


void *
isc_dns_resolve_cb(oop_source *UNUSED(src),
		   struct timeval UNUSED(tv),
		   void *user)
{
  struct isc_scb_internal *session = user;
  struct hostent *hp;
  isc_resolve_done_cb *cb;

  union isc_address *ia = session->pub.raddr;

  hp = CHOOSE_IP4OR6(ia->saddr,
		     gethostbyaddr((char *) &ia->saddr.sa_in.sin_addr,
				   sizeof(ia->saddr.sa_in.sin_addr),
				   ia->saddr.sa.sa_family),
		     gethostbyaddr((char *) &ia->saddr.sa_in6.sin6_addr,
				   sizeof(ia->saddr.sa_in6.sin6_addr),
				   ia->saddr.sa.sa_family));

  cb = session->resolve_callback;
  session->resolve_callback = NULL;

  if (hp)
  {
    s_crea_str(&session->pub.remote, hp->h_name);
    return cb(&session->pub, isc_resolve_ok, 0);
  }
  else
  {
    s_crea_str(&session->pub.remote, isc_getipnum(ia, NULL, 0));
    return cb(&session->pub, isc_resolve_h_errno, h_errno);
  }
}


void *
isc_dns_resolve_adns_cb(oop_adapter_adns *UNUSED(adapter),
			adns_answer *answer,
			void *user)
{
  struct isc_scb_internal *session = user;
  isc_resolve_done_cb *cb;
  void *res = NULL;

  cb = session->resolve_callback;
  session->resolve_callback = NULL;

  if (answer->status == adns_s_ok && answer->nrrs > 0)
  {
    s_crea_str(&session->pub.remote, answer->rrs.str[0]);
    res = cb(&session->pub, isc_resolve_ok, 0);
  }
  else
  {
    s_crea_str(&session->pub.remote,
	       isc_getipnum(session->pub.raddr, NULL, 0));
    res = cb(&session->pub, isc_resolve_adns_error, answer->status);
  }
  free(answer);
  return res;
}

int
isc_resolve_remote(struct isc_scb *scb,
		   isc_resolve_done_cb *callback)
{
  int errcode;
  struct isc_scb_internal *session = (struct isc_scb_internal*)scb;
  oop_source *source = session->pub.master->event_source;

  session->adns_query = oop_adns_submit_reverse(
      scb->master->adns, &errcode,
      &scb->raddr->saddr.sa,
      adns_r_ptr,
      0,
      isc_dns_resolve_adns_cb,
      session);

  if (session->adns_query == NULL)
  {
      /* adns does not yet support IPv6 addresses, so if this is an
	 IPv6 socket we have to use the old blocking method of host
	 name lookup. */
      if (errcode == ENOSYS)
	  source->on_time(source, OOP_TIME_NOW, isc_dns_resolve_cb, session);
      else
      {
	  assert(errcode);
	  return errcode;
      }
  }

  session->resolve_callback = callback;
  return 0;
}


int isc_getportnum(union isc_address *ia)
{
  return CHOOSE_IP4OR6(ia->saddr,
		       ntohs(ia->saddr.sa_in.sin_port),
		       ntohs(ia->saddr.sa_in6.sin6_port));
}
