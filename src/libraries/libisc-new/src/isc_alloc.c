/*
** isc_alloc.c                               ISC storage allocation routines
**
** Copyright (C) 1991, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910305 pen      moved into separate file
** (See ChangeLog for recent history)
*/

#include <sys/types.h>
#include <sys/socket.h>
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif
#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#ifdef HAVE_STRING_H
#  include <string.h>
#else
#  include <strings.h>
#endif
#ifndef NULL
#  include <stdio.h>
#endif
#include <time.h>
/* The order between inttypes.h and stdint.h is mandated by autoconf-2.57. */
#if HAVE_INTTYPES_H
#  include <inttypes.h>
#else
#  if HAVE_STDINT_H
#    include <stdint.h>
#  endif
#endif

#include "oop.h"
#include "adns.h"
#include "oop-adns.h"

#include "s-string.h"

#include "isc.h"
#include "intern.h"

/*
** Some magic numbers
*/
#ifdef USE_MALLOC_GUARDS

#define ISC_MAGIC_ALLOC	0x12F54ACEu
#define ISC_MAGIC_FREE  0xEE47A37Fu

/* A union of "all types", used to get the maximum alignment needed
   for any type. */
union overhead {

    /* We prefer to store stuf in a size_t, since we are storing a
       size.  Use an unsigned int if size_t isn't available.  */
#  ifdef HAVE_SIZE_T
    size_t val;
    unsigned int a;
#  else
    unsigned int val;
#  endif

    void *b;
    long c;
    long *d;
    long long e;
    long long *f;
    float g;
    double h;
    void (*i)(void);
    long double j;

#  ifdef HAVE_INTPTR_T
    intptr_t k;
#  endif

#  ifdef HAVE_INTMAX_T
    intmax_t l;
#  endif

};

#  define OVERHEAD (sizeof(union overhead))

#else

union overhead;
#  define OVERHEAD (0)

#endif

/*
** Pointers to functions to handle storage allocation
*/
static void * (*isc_mallocfn)(size_t size) = NULL;
static void * (*isc_reallocfn)(void *bufp, size_t size) = NULL;
static void (*isc_freefn)(void *bufp) = NULL;


/*
** Define functions to handle storage allocation
*/
void
isc_setallocfn(void * (*mallocfn)(size_t size),
             void * (*reallocfn)(void *buf, size_t size),
	     void   (*freefn)(void *buf))
{
  isc_mallocfn  = mallocfn;
  isc_reallocfn = reallocfn;
  isc_freefn    = freefn;
}



void *
isc_malloc(size_t size)
{
  union overhead *buf;


  if (isc_mallocfn)
    buf = (*isc_mallocfn)(size + OVERHEAD);
  else
  {
    buf = malloc(size + OVERHEAD);
    if (!buf)
      isc_abort("isc_malloc");
  }

#ifdef USE_MALLOC_GUARDS
  buf->val = ISC_MAGIC_ALLOC;
  buf++;
#endif

  return buf; 
}


void *
isc_realloc(void *oldbuf, size_t size)
{
  union overhead *buf;


  if (!oldbuf)
    return isc_malloc(size);

  buf = oldbuf;

#ifdef USE_MALLOC_GUARDS
  buf--;
  if (buf->val != ISC_MAGIC_ALLOC)
    isc_abort("isc_realloc");

  buf->val = ISC_MAGIC_FREE;
#endif
  
  if (isc_reallocfn)
    buf = (*isc_reallocfn)(buf, size + OVERHEAD);
  else
  {
    buf = realloc(buf, size + OVERHEAD);
    if (!buf)
      isc_abort("isc_realloc");
  }

#ifdef USE_MALLOC_GUARDS
  buf->val = ISC_MAGIC_ALLOC;
  buf++;
#endif

  return (void *) buf;
}



void
isc_free(void *buf)
{
  union overhead *ibuf;

  
  if (!buf)
    isc_abort("isc_free");
    
  ibuf = buf;

#ifdef USE_MALLOC_GUARDS
  ibuf--;
  if (ibuf->val != ISC_MAGIC_ALLOC)
    isc_abort("isc_free");

  ibuf->val = ISC_MAGIC_FREE;
#endif
  
  if (isc_freefn)
  {
    (*isc_freefn)(ibuf);
    return;
  }
  else
    free(ibuf);
}
