1. What is LysKOM?
==================

LysKOM has a lot in common with netnews, but LysKOM is intended for
local discussions (instead of worldwide).  LysKOM consists of a server
process and several client programs.  The server process maintains a
data base of persons, conferences and articles.  The clients connect
to the server and let the users browse the database for unread
articles.

LysKOM is much faster than netnews - almost as fast as irc or ICQ! -
but like in netnews the articles are saved so that you don't have to
be logged in to receive the news.

As mentioned above, you need a client to be able to do something
useful with LysKOM.  At this time there are several clients available.
The most popular client is written in elisp and requires Gnu Emacs or
XEmacs to run. There are also web clients and a MS Windows client. See
<URL:http://www.lysator.liu.se/lyskom/> for availability of clients.

A LysKOM server has been running at Lysator since July 25th 1990.  You
can connect to it and test it out if you like to get a feel for what
LysKOM is.  The hostname of the server is kom.lysator.liu.se.  Of
course, you need a client to test it.  On that particular server most
of the discussions are done in Swedish.

See the file NEWS for information about changes, or the various
ChangeLogs for more detailed information.

The lyskom-server distribution, of which this README file is a part,
only contains the server.  You must obtain the clients separately.


2. Documentation
================

doc/lyskomd.texi	Instructions for maintaining a LysKOM server.
			Also contains a chapter for hackers that want
			to extend or modify the server.

doc/Protocol-A.texi	Documentation of the protocol used between the
			clients and the LysKOM server.

NEWS			What is new in this release?
README			This file.
INSTALL			Generic install instructions (see below for
			lyskomd-specific instructions).
HACKING			Information that may be useful if you want to
			change lyskomd.
COPYING			The license for lyskomd (GNU GPL).
AUTHORS			List of authors.


3. Installing or upgrading a LysKOM installation
================================================

   1. Select a location where you want the LysKOM database and server
      binaries to reside.  We recommend something like /usr/lyskom
      (the default), /usr/local/lyskom or /opt/lyskom.

      WARNING: Do not place the database on an NFS-mounted directory
      that is mounted with "intr"!  If you do, the system calls that
      writes the database may return a failure and set errno to
      EINTR.  lyskomd does not yet handle that appropriately.  See bug
      847 for more details and the current status of this problem.
      <URL:http://bugzilla.lysator.liu.se/show_bug.cgi?id=847>.

   2. From the top directory of the server sources, run configure:

	$ ./configure -C --prefix=/usr/local/lyskom

      The prefix defaults to /usr/lyskom.  There are many other
      options that you can give to the configure script.  Most of them
      are documented in the generic INSTALL file.  There are a few
      LysKOM-specific options, but except for --with-language they are
      only useful if you want to debug lyskomd.  See the HACKING file
      for more information about them.

      You can use --with-language=sv to select that a Swedish database
      should be installed (if no previous database exists).  By
      default, an English database will be installed.  This only
      affects the names of a few conferences and the administrator.
      You can rename them manually once the server is running.

      The "-C" option tells configure to cache configuration values in
      config.cache.  Since the lyskomd distribution uses more than one
      configure scripts, this will speed up the configure process.

   3. Compile it:

	$ make

   4. If you like, you can run the test suite:

	$ make check

      There should be no unexpected errors.  It is normal that some
      unsupported tests and expected errors are reported.  You need
      DejaGnu 1.4.2 and Python to run the test suite.  DejaGnu in turn
      needs TCL and expect.  They can be downloaded from:

	ftp://prep.ai.mit.edu/pub/gnu/dejagnu/dejagnu-1.4.2.tar.gz
	ftp://ftp.scriptics.com/pub/tcl/tcl8_3/tcl8.3.4.tar.gz
	http://expect.nist.gov/src/expect-5.38.0.tar.gz
	http://www.python.org/download/

      Please note that expect-5.43.0 has a bug that prevents it from
      running the testsuite.  A patch that fixes the problem is
      available here:

        <ftp://ftp.lysator.liu.se/pub/lyskom/server/
	 expect-5.43.0-zero-len-write.patch>

      Expect version 5.44.1.1 introduced another bug that was fixed in
      5.45, so the 5.44 series cannot be used unless the fix is
      backported.  (Debian backported the fix in their 5.44.1.15-3
      version.)

   5. Create a user that should own the database files.  This file
      assumes that the user is named "lyskom".

   6. If you are upgrading, you must stop the running lyskomd, using
      the installed "komrunning" program:

	$ su lyskom
	$ /usr/lyskom/sbin/komrunning stop

      (If you are upgrading from a version older than 2.1.0, the
      komrunning program will be residing in /usr/lyskom/bin.)

   7. If you are upgrading from 2.0.7 or older to 2.1.0 or newer, run
      these commands to adjust to the new filesystem layout (see NEWS
      for a complete list of everything that changed; this only moves
      the database, config file and log files from the old to the new
      layout):

	$ su lyskom
	$ cd /usr/lyskom
	$ mv etc/config etc/lyskomd.conf
	$ mkdir -p var/lyskomd/db
	$ mv db/* var/lyskomd/db/
	$ rmdir db
	$ mv etc/server-log var/lyskomd.log
	$ mv etc/lyskomd-log var/lyskomd.stats
	$ mv etc/memory-usage var/lyskomd.memory
	$ cp etc/status var/lyskomd/db/status

      It might be a good idea to remove the old files in bin, or at
      least move them to a location where you won't run them by
      mistake.

   8. As "lyskom", install it:

	$ su lyskom
	$ make install

      This will install the server and documentation.  It will also
      install an empty database, unless it finds an old database.  The
      empty database will contain four standard conferences and one
      privileged user, "Administrator (for) LysKOM", with password
      "gazonk".  See "Database versions" below if you have an old
      database -- you may have to convert it.

      A sample config file will be installed in etc/lyskomd.conf,
      unless it already exists.  If you are upgrading to a new version
      of lyskomd you may have to edit the config file.  The current
      sample config file is in run-support/config.

      The configuration for aux-items will be installed in
      etc/aux-items.conf.  It will be overwritten even if it has been
      edited locally.

   9. If you are upgrading from 2.0.7 or older to 2.1.0 or newer you
      need to update the cron entries, so that they run the binaries
      from sbin instead of bin.  Once that is done, you can remove the
      old etc/status file.

  10. Make sure updateLysKOM is run regularly by user lyskom.  The
      recommended way is to add a line such as this to the cron file
      of the lyskom user:

	0,10,20,30,40,50 * * * * /usr/lyskom/sbin/updateLysKOM

      If your cron does not support per-user cron files you will have
      to arrange this in some other way.

      The updateLysKOM will occasionally output a message to stderr
      and exit with a non-zero exit status.  It expects the cron to
      send a mail to a LysKOM administrator with the output from the
      program.  Most sane cron systems does so.  You may want to
      create a .forward file for user "lyskom".

  11. If you are upgrading, you must run komrunning again to remove
      the lock it created: 

	$ su lyskom
	$ /usr/lyskom/sbin/komrunning start

      (Despite its name, this does not actually start lyskomd.  See
      the next step.)

  12. Wait for updateLysKOM to start the server, or run updateLysKOM
      manually (as user "lyskom").  You are ready.

      If you are upgrading from 2.1.0 or 2.1.1 you might see entries
      similar to this in the log:

	fparse_read_range_0: discarded out-of-order local number 8
	probably introduced by bug 1121

      This means that the server has detected an error in the database
      introduced by 2.1.0 or 2.1.1.  The error will be fixed
      automatically once the affected person logs on.  This message is
      harmless.

  13. Read the documentation in doc/lyskomd.info, especially the
      Administration chapter.  It tells you how to shut down and
      restart lyskomd.  LysKOM needs your attention once in a while.
      Here at Lysator we expunge old texts (with "dbck -g")
      approximately twice a year.

  14. We would like to know about where LysKOM servers are running.
      If you start a server, please mail "kom@lysator.liu.se", and
      tell us that you are running a server.  (You are very welcome to
      tell more about you, but that's not necessary).


4. Database Versions -- you may have to convert the database
============================================================

The database format has not changed in the 2.0.1 through 2.0.6
releases.  However, there was a bug in the 2.0.0 release that may have
caused the database to contain errors.  Most of them can be fixed by
dbck, but some cannot yet be fixed; a release with an improved dbck
might be created in the future.  Please run this command if you are
upgrading from the 2.0.0 release:

	dbck -i

lyskomd version 2.0.0 uses a new file format for the database. If you
have been running earlier versions of lyskomd you must convert the
database by issuing the following command:

	dbck -o 2 -F

You must, of course, use version 2.0.0 or later of dbck to do the
conversion.  You can convert back the database to the old format with

	dbck -o 1 -F        (for lyskomd version 1.9.0)
	dbck -o 0 -F        (for ancient lyskomd versions)


5. Contact information
======================

If you find a bug, please take a moment to check the bug database at
http://bugzilla.lysator.liu.se/ to make sure that it isn't an old bug.
If you find your bug there, and feel that you have more information
about it, please make an "additional comment".  If the bug is
important for you, please tell us so by voting for it.  If you cannot
find the bug, please enter a bug report there.

If you are unable to use Bugzilla, or if you don't want the
information you have to be public knowledge, you can send your bug
report to the email address bug-lyskom@lysator.liu.se.

The latest release of the server can always be found from

	ftp://ftp.lysator.liu.se/pub/lyskom/server/

This file and almost the entire lyskomd server is 
Copyright (C) 1991-2003 Lysator Academic Computer Association
See also the AUTHORS file.
