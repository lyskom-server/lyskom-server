# $Id: Makefile.am,v 1.11 2003/08/16 11:29:11 ceder Exp $
# Copyright (C) 1998  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 
#

include $(top_srcdir)/scripts/common.make

LANGUAGE_SUFFIX=@LANGUAGE_SUFFIX@

# lyskomd-data and lyskomd-texts should be in db_DATA, but we don't
# want to install them if they already exist.  This is the same
# situation that we have in run-support/Makefile.am.
db_DATA = 
EXTRA_DIST = lyskomd-data lyskomd-texts lyskomd-data-en \
	number.txt

install-data-local: installdirs
	if test -f $(DESTDIR)$(dbdir)/lyskomd-data \
	|| test -f $(DESTDIR)$(dbdir)/lyskomd-backup \
	|| test -f $(DESTDIR)$(dbdir)/lyskomd-texts; \
	then \
	    echo installed database found; \
	else  \
	    $(INSTALL_DATA) $(srcdir)/lyskomd-data$(LANGUAGE_SUFFIX) \
		$(DESTDIR)$(dbdir)/lyskomd-data || exit 1 ; \
	    $(INSTALL_DATA) $(srcdir)/lyskomd-texts \
		$(DESTDIR)$(dbdir)/lyskomd-texts || exit 1; \
	    $(INSTALL_DATA) $(srcdir)/number.txt \
		$(DESTDIR)$(dbdir)/number.txt || exit 1; \
	fi

uninstall-local:
	if cmp $(srcdir)/lyskomd-data$(LANGUAGE_SUFFIX) \
	        $(DESTDIR)$(dbdir)/lyskomd-data \
	    && cmp $(srcdir)/lyskomd-texts \
	        $(DESTDIR)$(dbdir)/lyskomd-texts \
	    && cmp $(srcdir)/number.txt \
	        $(DESTDIR)$(dbdir)/number.txt ; \
	then \
	    $(RM) $(DESTDIR)$(dbdir)/lyskomd-data \
		$(DESTDIR)$(dbdir)/lyskomd-texts \
		$(DESTDIR)$(dbdir)/number.txt; \
	else \
	    echo '*******************************************************' ; \
	    echo 'Cowardly refusing to uninstall the modified database in' ; \
	    echo $(DESTDIR)$(dbdir) ; \
	    echo '*******************************************************' ; \
	fi
